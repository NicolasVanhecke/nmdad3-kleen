angular.module('app.controllers',[])

    .controller('DashCtrl', function($rootScope, $scope, User) {
        $scope.userreports = User.reports({id:1});
        //$scope.reports = Users.reports({id:1});
        //$scope.stats = Leaderboard.getStats();
    })

    .controller('ReportsCtrl', function($scope, User) {
        $scope.allusers = User.query();
        $scope.user = User.get({id:1});
        $scope.userreports = User.reports({id:1});

        //$scope.ureps = Api.UserReports.query({id: 1});
        //$scope.users = Api.Users.query();
        //$scope.userreport = Api.UserReport.query({id: 1, reportId: 1});
    })
    .controller('ReportsDetailCtrl', function($scope, $stateParams, $cordovaGeolocation, Report) {
        $scope.reportdetail = Report.query({id: 1}, {reportId: $stateParams.reportId});
        $scope.reportstates = Report.reportstates({id: 1}, {reportId: $stateParams.reportId});

        // G Maps
        // ======
        var options = {timeout: 10000, enableHighAccuracy: true};

        $cordovaGeolocation.getCurrentPosition(options).then(function(position){

            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            var mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

            //Wait until the map is loaded
            google.maps.event.addListenerOnce($scope.map, 'idle', function(){

                var marker = new google.maps.Marker({
                    map: $scope.map,
                    animation: google.maps.Animation.DROP,
                    position: latLng
                });

            });

        }, function(error){
            console.log("Could not get location");
        });
    })

    .controller('CameraCtrl', function($scope, $log, $ionicPlatform, $cordovaCamera, $cordovaGeolocation, Report) {
        // ViewModel
        // =========
        var vm = this;
        vm.report=new Report({id:1});

        vm.getPhoto = getPhoto;
//        vm.lastPhoto = '';
        vm.title = 'Meld zwerfvuil';

        // Functions
        // =========
        function getPhoto() {
            var cameraOptions = {
                quality: 75,
                targetWidth: 320,
                targetHeight: 320,
                saveToPhotoAlbum: false
            };

            $cordovaCamera
                .getPicture(cameraOptions)
                .then(getPhotoSuccess, getPhotoError);
        }

        function getPhotoSuccess(imageUri) {
            $log.log(imageUri);

            vm.report.photo = imageUri;
        }

        function getPhotoError(error) {
            $log.error(error);
            vm.report.photo = 'error';
        }

        // Google Maps
        // ===========
        var options = {timeout: 10000, enableHighAccuracy: true};

        $cordovaGeolocation.getCurrentPosition(options).then(function(position){

            vm.report.location_lat =position.coords.latitude;
            vm.report.location_long = position.coords.longitude;

            //var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            //
            //var mapOptions = {
            //    center: latLng,
            //    zoom: 15,
            //    mapTypeId: google.maps.MapTypeId.ROADMAP,
            //    disableDefaultUI: true
            //};
            //
            //$scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
            //
            ////Wait until the map is loaded
            //google.maps.event.addListenerOnce($scope.map, 'idle', function(){
            //
            //    var marker = new google.maps.Marker({
            //        map: $scope.map,
            //        animation: google.maps.Animation.DROP,
            //        position: latLng
            //    });
            //
            //});

        }, function(error){
            console.log("Could not get location");
        });

        //report opslaan
        vm.saveReport = function(){
            Report.save(
                {id:1},
                {report:vm.report},
                function(report){
                    vm.report=new Report();
                }
            )
        };

    })

    .controller('LeaderbCtrl', function($scope, Leaderboard) {
        //$scope.users = Users.query();
        //$scope.stats = Leaderboard.getStats();

        // Call the async method and then do stuff with what is returned inside our own then function
        Leaderboard.async().then(function(d) {
            $scope.stats = d;
        });
    })

    .controller('AccountCtrl', function($scope, $http, CurrentUser, User ) {
        CurrentUser.async().then(function(d) {
            $scope.user = d;
        });

        $scope.userreports = User.reports({id:1});

    })

    //.controller('AccountCtrl', function($scope, $http, Report, reportFactory) {
        //$scope.status;
        //$scope.reports;
        //
        //getUserReports();
        //
        //function getUserReports() {
        //    reportFactory.getReports()
        //        .success(function (reps) {
        //            $scope.reports = reps;
        //        })
        //        .error(function (error) {
        //            $scope.status = 'Unable to load report data: ' + error.message;
        //        });
        //}
        //
        ////$scope.updateCustomer = function (id) {
        ////    var cust;
        ////    for (var i = 0; i < $scope.customers.length; i++) {
        ////        var currCust = $scope.customers[i];
        ////        if (currCust.ID === id) {
        ////            cust = currCust;
        ////            break;
        ////        }
        ////    }
        ////
        ////    dataFactory.updateCustomer(cust)
        ////        .success(function () {
        ////            $scope.status = 'Updated Customer! Refreshing customer list.';
        ////        })
        ////        .error(function (error) {
        ////            $scope.status = 'Unable to update customer: ' + error.message;
        ////        });
        ////};
        //
        //$scope.postReport = function () {
        //
        //    var report = new Report({
        //        type: 'Vuilzakken',
        //        description: 'Nieuwe melding via Ionic',
        //        location_lat: 50.979797,
        //        location_long: 3.784766,
        //        photo: 'photostring'
        //    });
        //
        //
        //
        //    //Fake customer data
        //    var rep = {
        //        type: 'Vuilzakken',
        //        description: 'Nieuwe melding via Ionic',
        //        location_lat: 50.979797,
        //        location_long: 3.784766,
        //        photo: 'photostring'
        //    };
        //
        //    reportFactory.postReport(rep)
        //        .success(function () {
        //            $scope.status = 'Inserted report!.';
        //            $scope.reports.push(rep);
        //        }).
        //    error(function(error) {
        //        $scope.status = 'Unable to insert report: ' + error.message;
        //    });
        //};
        //
        //$scope.deleteCustomer = function (id) {
        //    dataFactory.deleteCustomer(id)
        //        .success(function () {
        //            $scope.status = 'Deleted Customer! Refreshing customer list.';
        //            for (var i = 0; i < $scope.customers.length; i++) {
        //                var cust = $scope.customers[i];
        //                if (cust.ID === id) {
        //                    $scope.customers.splice(i, 1);
        //                    break;
        //                }
        //            }
        //            $scope.orders = null;
        //        })
        //        .error(function (error) {
        //            $scope.status = 'Unable to delete customer: ' + error.message;
        //        });
        //};
    //})

    .controller('SecurityLoginCtrl', function($rootScope, $scope, AuthService) {
        var vm = this;

        vm.loginDetails = {};

        vm.login = function() {
            console.log(vm.loginDetails);
            AuthService.login(vm.loginDetails);
        };

    })
;