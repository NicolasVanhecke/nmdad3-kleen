;(function(){
    'use strict';

    angular
        .module('app')
        .factory('AuthService', AuthService)
        .factory('AuthInterceptor', AuthInterceptor)
        .service('Session', function () {
            this.create = function (user) {
                this.user=user;
            };
            this.destroy = function () {
                this.user = null;
            };
            return this;
        })
    ;

    /** @ngInject */
    function AuthService(apipath, $http, $base64, $log, Session) {
        var authService = {};
        //Session.destroy();
        authService.login = function (credentials) {

            var req = {
                method: 'POST',
                url: apipath+'/auth/login',
                headers: {
                    'Authorization': 'Basic ' + $base64.encode(credentials.username + ':' + credentials.password),
                    'Accept': 'application/json;odata=verbose'
                }
            }

            return $http(req)
                .then(function (res) {
                    //save info in our local storage...
                    Session.create(res.data);
                    return res.data;
                });
        };

        authService.register = function (user){
            var req = {
                method: 'POST',
                url: apipath+'/auth/register/',
                data:user,
                headers: {
                    'Accept': 'application/json;odata=verbose'
                }
            };

            return $http(req)
                .then(function(){
                    return;
                });
        };

        authService.isAuthenticated = function () {

            return Session.user != null;
        };

        authService.isGuest = function () {

            return Session.user == null;
        }

        return authService;
    }

    /** @ngInject */
    function AuthInterceptor($rootScope, $q, Session) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                if (Session.user) {
                    config.headers.Authorization = 'Basic ' + $base64.encode(credentials.username + ':' + credentials.password);
                }
                return config;
            },
            responseError: function (response) {

            }
        };
    }
})();