/**
 * Created by Nicolas on 13/12/15.
 */

angular.module('app')
    .provider('User', function() {
        this.$get = ['$resource', function($resource) {
            //return $resource('http://www.nmdad3.local/app_dev.php/api/v1/users/:id',{id:'@id'});
            return $resource('http://www.nmdad3.local/app_dev.php/api/v1/users/:id', {id:'@id'}, {
                    reports:{ url:'http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports', isArray:true }
                    //createReport:{ url:'http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports', method:POST }
            });
        }]
    })

    .provider('Report', function() {
        this.$get = ['$resource', function($resource) {
            return $resource('http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports/:reportId', {id:'@id', reportId:'@reportId'}, {
                reportstates:{ url:'http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports/:reportId/reportstates', isArray:true }
            });
        }]
    })

    //.provider('ReportStates', function() {
    //    this.$get = ['$resource', function($resource) {
    //        return $resource('http://www.nmdad3.local/app_dev.php/api/v1/reports/:reportId/reportstates', {reportId:'@reportId'});
    //    }]
    //})

    // $http returns a promise, which has a then function, which also returns a promise.
    // The return value gets picked up by the then in the controller.
    // Return the promise to the controller.
    .factory('Leaderboard', function($http) {
        var Leaderboard = {
            async: function() {
                var promise = $http.get('http://www.nmdad3.local/app_dev.php/api/v1/leaderboards').then(function (response) {
                    return response.data;
                });
                return promise;
            }
        };
        return Leaderboard;
    })

    .factory('CurrentUser', function($http) {
        var CurrentUser = {
            async: function() {
                var promise = $http.get('http://www.nmdad3.local/app_dev.php/api/v1/users/1', {id:'@id'}).then(function (response) {
                    return response.data;
                });
                return promise;
            }
        };
        return CurrentUser;
    })


    //.factory('reportFactory', ['$http', function($http) {
    //
    //    //var urlBase = 'http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports';
    //    var urlBase = 'http://www.nmdad3.local/app_dev.php/api/v1/users/';
    //    var reportFactory = {};
    //
    //    reportFactory.getReports = function (id) {
    //        return $http.get(urlBase + '1' + '/reports');
    //    };
    //
    //    reportFactory.postReport = function (id, rep) {
    //        return $http.post(urlBase + '1' + '/reports', rep);
    //        //return $http.post(urlBase + id + '/reports', rep);
    //    };
    //
    //    //reportFactory.updateCustomer = function (cust) {
    //    //    return $http.put(urlBase + '/' + cust.ID, cust)
    //    //};
    //    //
    //    //reportFactory.deleteCustomer = function (id) {
    //    //    return $http.delete(urlBase + '/' + id);
    //    //};
    //
    //    return reportFactory;
    //}])

;


