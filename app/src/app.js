// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', [
    'ionic',
    'ngCordova',
    'ngResource',
    'base64',
    'app.controllers',
  //  'app.services',

])
.run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $compileProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};

    // Allow 'app:' as protocol (for use in Hybrid Mobile apps)
    $compileProvider
        .aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|app):/)
        .imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|app):|data:image\/)/)
    ;

    // Basic Auth
    var username = 'nicolasvh',
        password = 'nicolasvh',
        credentials = window.btoa(username + ':' + password);
    $httpProvider.defaults.headers.common['Authorization'] = 'Basic ' + credentials;



    $httpProvider.interceptors.push(['$injector', function ($injector) {
        return $injector.get('AuthInterceptor')
    }]);
    $stateProvider

    // setup login page
    .state('security', {
        url: '/security/login',
        templateUrl: 'templates/security/login.html',
        controller: 'SecurityLoginCtrl as securityLoginCtrl'
    })

    // setup an abstract state for the tabs directive
    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
    })

    // Each tab has its own nav history stack:
    .state('tab.dash', {
        url: '/dash',
        views: {
            'tab-dash': {
                templateUrl: 'templates/dash/tab-dash.html',
                controller: 'DashCtrl as vm'
            }
        }
    })

    .state('tab.reports', {
        url: '/reports',
        views: {
            'tab-reports': {
                controller: 'ReportsCtrl as vm',
                templateUrl: 'templates/reports/tab-reports.html'
            }
        }
    })
    .state('tab.reports_detail', {
        url: '/reports/:reportId',
        views: {
            'tab-reports': {
                controller: 'ReportsDetailCtrl as vm',
                templateUrl: 'templates/reports/reports-detail.html'
            }
        }
    })

    .state('tab.camera', {
        url: '/camera',
        views: {
            'tab-camera': {
                controller: 'CameraCtrl as vm',
                templateUrl: 'templates/camera/tab-camera.html'
            }
        }
    })

    .state('tab.leaderb', {
        url: '/leaderb',
        views: {
            'tab-leaderb': {
                controller: 'LeaderbCtrl as vm',
                templateUrl: 'templates/leaderb/tab-leaderb.html'
            }
        }
    })

    .state('tab.account', {
        url: '/account',
        views: {
            'tab-account': {
                controller: 'AccountCtrl as vm',
                templateUrl: 'templates/account/tab-account.html'
            }
        }
    })

    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/dash');

}).constant('apipath','http://www.nmdad3.local/app_dev.php/api/v1');
