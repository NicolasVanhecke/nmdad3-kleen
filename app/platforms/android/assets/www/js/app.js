// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', [
    'ionic',
    'ngCordova',
    'ngResource',
    'base64',
    'app.controllers',
  //  'app.services',

])
.run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $compileProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};

    // Allow 'app:' as protocol (for use in Hybrid Mobile apps)
    $compileProvider
        .aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|app):/)
        .imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|app):|data:image\/)/)
    ;

    // Basic Auth
    var username = 'nicolasvh',
        password = 'nicolasvh',
        credentials = window.btoa(username + ':' + password);
    $httpProvider.defaults.headers.common['Authorization'] = 'Basic ' + credentials;



    $httpProvider.interceptors.push(['$injector', function ($injector) {
        return $injector.get('AuthInterceptor')
    }]);
    $stateProvider

    // setup login page
    .state('security', {
        url: '/security/login',
        templateUrl: 'templates/security/login.html',
        controller: 'SecurityLoginCtrl as securityLoginCtrl'
    })

    // setup an abstract state for the tabs directive
    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
    })

    // Each tab has its own nav history stack:
    .state('tab.dash', {
        url: '/dash',
        views: {
            'tab-dash': {
                templateUrl: 'templates/dash/tab-dash.html',
                controller: 'DashCtrl as vm'
            }
        }
    })

    .state('tab.reports', {
        url: '/reports',
        views: {
            'tab-reports': {
                controller: 'ReportsCtrl as vm',
                templateUrl: 'templates/reports/tab-reports.html'
            }
        }
    })
    .state('tab.reports_detail', {
        url: '/reports/:reportId',
        views: {
            'tab-reports': {
                controller: 'ReportsDetailCtrl as vm',
                templateUrl: 'templates/reports/reports-detail.html'
            }
        }
    })

    .state('tab.camera', {
        url: '/camera',
        views: {
            'tab-camera': {
                controller: 'CameraCtrl as vm',
                templateUrl: 'templates/camera/tab-camera.html'
            }
        }
    })

    .state('tab.leaderb', {
        url: '/leaderb',
        views: {
            'tab-leaderb': {
                controller: 'LeaderbCtrl as vm',
                templateUrl: 'templates/leaderb/tab-leaderb.html'
            }
        }
    })

    .state('tab.account', {
        url: '/account',
        views: {
            'tab-account': {
                controller: 'AccountCtrl as vm',
                templateUrl: 'templates/account/tab-account.html'
            }
        }
    })

    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/dash');

}).constant('apipath','http://www.nmdad3.local/app_dev.php/api/v1');

;(function(){
    'use strict';

    angular
        .module('app')
        .factory('AuthService', AuthService)
        .factory('AuthInterceptor', AuthInterceptor)
        .service('Session', function () {
            this.create = function (user) {
                this.user=user;
            };
            this.destroy = function () {
                this.user = null;
            };
            return this;
        })
    ;

    /** @ngInject */
    function AuthService(apipath, $http, $base64, $log, Session) {
        var authService = {};
        //Session.destroy();
        authService.login = function (credentials) {

            var req = {
                method: 'POST',
                url: apipath+'/auth/login',
                headers: {
                    'Authorization': 'Basic ' + $base64.encode(credentials.username + ':' + credentials.password),
                    'Accept': 'application/json;odata=verbose'
                }
            }

            return $http(req)
                .then(function (res) {
                    //save info in our local storage...
                    Session.create(res.data);
                    return res.data;
                });
        };

        authService.register = function (user){
            var req = {
                method: 'POST',
                url: apipath+'/auth/register/',
                data:user,
                headers: {
                    'Accept': 'application/json;odata=verbose'
                }
            };

            return $http(req)
                .then(function(){
                    return;
                });
        };

        authService.isAuthenticated = function () {

            return Session.user != null;
        };

        authService.isGuest = function () {

            return Session.user == null;
        }

        return authService;
    }

    /** @ngInject */
    function AuthInterceptor($rootScope, $q, Session) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                if (Session.user) {
                    config.headers.Authorization = 'Basic ' + $base64.encode(credentials.username + ':' + credentials.password);
                }
                return config;
            },
            responseError: function (response) {

            }
        };
    }
})();
angular.module('app.controllers',[])

    .controller('DashCtrl', function($rootScope, $scope, User) {
        $scope.userreports = User.reports({id:1});
        //$scope.reports = Users.reports({id:1});
        //$scope.stats = Leaderboard.getStats();
    })

    .controller('ReportsCtrl', function($scope, User) {
        $scope.allusers = User.query();
        $scope.user = User.get({id:1});
        $scope.userreports = User.reports({id:1});

        //$scope.ureps = Api.UserReports.query({id: 1});
        //$scope.users = Api.Users.query();
        //$scope.userreport = Api.UserReport.query({id: 1, reportId: 1});
    })
    .controller('ReportsDetailCtrl', function($scope, $stateParams, $cordovaGeolocation, Report) {
        $scope.reportdetail = Report.query({id: 1}, {reportId: $stateParams.reportId});
        $scope.reportstates = Report.reportstates({id: 1}, {reportId: $stateParams.reportId});

        // G Maps
        // ======
        var options = {timeout: 10000, enableHighAccuracy: true};

        $cordovaGeolocation.getCurrentPosition(options).then(function(position){

            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            var mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

            //Wait until the map is loaded
            google.maps.event.addListenerOnce($scope.map, 'idle', function(){

                var marker = new google.maps.Marker({
                    map: $scope.map,
                    animation: google.maps.Animation.DROP,
                    position: latLng
                });

            });

        }, function(error){
            console.log("Could not get location");
        });
    })

    .controller('CameraCtrl', function($scope, $log, $ionicPlatform, $cordovaCamera, $cordovaGeolocation, Report) {
        // ViewModel
        // =========
        var vm = this;
        vm.report=new Report({id:1});

        vm.getPhoto = getPhoto;
//        vm.lastPhoto = '';
        vm.title = 'Meld zwerfvuil';

        // Functions
        // =========
        function getPhoto() {
            var cameraOptions = {
                quality: 75,
                targetWidth: 320,
                targetHeight: 320,
                saveToPhotoAlbum: false
            };

            $cordovaCamera
                .getPicture(cameraOptions)
                .then(getPhotoSuccess, getPhotoError);
        }

        function getPhotoSuccess(imageUri) {
            $log.log(imageUri);

            vm.report.photo = imageUri;
        }

        function getPhotoError(error) {
            $log.error(error);
            vm.report.photo = 'error';
        }

        // Google Maps
        // ===========
        var options = {timeout: 10000, enableHighAccuracy: true};

        $cordovaGeolocation.getCurrentPosition(options).then(function(position){

            vm.report.location_lat =position.coords.latitude;
            vm.report.location_long = position.coords.longitude;

            //var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            //
            //var mapOptions = {
            //    center: latLng,
            //    zoom: 15,
            //    mapTypeId: google.maps.MapTypeId.ROADMAP,
            //    disableDefaultUI: true
            //};
            //
            //$scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
            //
            ////Wait until the map is loaded
            //google.maps.event.addListenerOnce($scope.map, 'idle', function(){
            //
            //    var marker = new google.maps.Marker({
            //        map: $scope.map,
            //        animation: google.maps.Animation.DROP,
            //        position: latLng
            //    });
            //
            //});

        }, function(error){
            console.log("Could not get location");
        });

        //report opslaan
        vm.saveReport = function(){
            Report.save(
                {id:1},
                {report:vm.report},
                function(report){
                    vm.report=new Report();
                }
            )
        };

    })

    .controller('LeaderbCtrl', function($scope, Leaderboard) {
        //$scope.users = Users.query();
        //$scope.stats = Leaderboard.getStats();

        // Call the async method and then do stuff with what is returned inside our own then function
        Leaderboard.async().then(function(d) {
            $scope.stats = d;
        });
    })

    .controller('AccountCtrl', function($scope, $http, CurrentUser, User ) {
        CurrentUser.async().then(function(d) {
            $scope.user = d;
        });

        $scope.userreports = User.reports({id:1});

    })

    //.controller('AccountCtrl', function($scope, $http, Report, reportFactory) {
        //$scope.status;
        //$scope.reports;
        //
        //getUserReports();
        //
        //function getUserReports() {
        //    reportFactory.getReports()
        //        .success(function (reps) {
        //            $scope.reports = reps;
        //        })
        //        .error(function (error) {
        //            $scope.status = 'Unable to load report data: ' + error.message;
        //        });
        //}
        //
        ////$scope.updateCustomer = function (id) {
        ////    var cust;
        ////    for (var i = 0; i < $scope.customers.length; i++) {
        ////        var currCust = $scope.customers[i];
        ////        if (currCust.ID === id) {
        ////            cust = currCust;
        ////            break;
        ////        }
        ////    }
        ////
        ////    dataFactory.updateCustomer(cust)
        ////        .success(function () {
        ////            $scope.status = 'Updated Customer! Refreshing customer list.';
        ////        })
        ////        .error(function (error) {
        ////            $scope.status = 'Unable to update customer: ' + error.message;
        ////        });
        ////};
        //
        //$scope.postReport = function () {
        //
        //    var report = new Report({
        //        type: 'Vuilzakken',
        //        description: 'Nieuwe melding via Ionic',
        //        location_lat: 50.979797,
        //        location_long: 3.784766,
        //        photo: 'photostring'
        //    });
        //
        //
        //
        //    //Fake customer data
        //    var rep = {
        //        type: 'Vuilzakken',
        //        description: 'Nieuwe melding via Ionic',
        //        location_lat: 50.979797,
        //        location_long: 3.784766,
        //        photo: 'photostring'
        //    };
        //
        //    reportFactory.postReport(rep)
        //        .success(function () {
        //            $scope.status = 'Inserted report!.';
        //            $scope.reports.push(rep);
        //        }).
        //    error(function(error) {
        //        $scope.status = 'Unable to insert report: ' + error.message;
        //    });
        //};
        //
        //$scope.deleteCustomer = function (id) {
        //    dataFactory.deleteCustomer(id)
        //        .success(function () {
        //            $scope.status = 'Deleted Customer! Refreshing customer list.';
        //            for (var i = 0; i < $scope.customers.length; i++) {
        //                var cust = $scope.customers[i];
        //                if (cust.ID === id) {
        //                    $scope.customers.splice(i, 1);
        //                    break;
        //                }
        //            }
        //            $scope.orders = null;
        //        })
        //        .error(function (error) {
        //            $scope.status = 'Unable to delete customer: ' + error.message;
        //        });
        //};
    //})

    .controller('SecurityLoginCtrl', function($rootScope, $scope, AuthService) {
        var vm = this;

        vm.loginDetails = {};

        vm.login = function() {
            console.log(vm.loginDetails);
            AuthService.login(vm.loginDetails);
        };

    })
;
/**
 * Created by Nicolas on 13/12/15.
 */

angular.module('app')
    .provider('User', function() {
        this.$get = ['$resource', function($resource) {
            //return $resource('http://www.nmdad3.local/app_dev.php/api/v1/users/:id',{id:'@id'});
            return $resource('http://www.nmdad3.local/app_dev.php/api/v1/users/:id', {id:'@id'}, {
                    reports:{ url:'http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports', isArray:true }
                    //createReport:{ url:'http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports', method:POST }
            });
        }]
    })

    .provider('Report', function() {
        this.$get = ['$resource', function($resource) {
            return $resource('http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports/:reportId', {id:'@id', reportId:'@reportId'}, {
                reportstates:{ url:'http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports/:reportId/reportstates', isArray:true }
            });
        }]
    })

    //.provider('ReportStates', function() {
    //    this.$get = ['$resource', function($resource) {
    //        return $resource('http://www.nmdad3.local/app_dev.php/api/v1/reports/:reportId/reportstates', {reportId:'@reportId'});
    //    }]
    //})

    // $http returns a promise, which has a then function, which also returns a promise.
    // The return value gets picked up by the then in the controller.
    // Return the promise to the controller.
    .factory('Leaderboard', function($http) {
        var Leaderboard = {
            async: function() {
                var promise = $http.get('http://www.nmdad3.local/app_dev.php/api/v1/leaderboards').then(function (response) {
                    return response.data;
                });
                return promise;
            }
        };
        return Leaderboard;
    })

    .factory('CurrentUser', function($http) {
        var CurrentUser = {
            async: function() {
                var promise = $http.get('http://www.nmdad3.local/app_dev.php/api/v1/users/1', {id:'@id'}).then(function (response) {
                    return response.data;
                });
                return promise;
            }
        };
        return CurrentUser;
    })


    //.factory('reportFactory', ['$http', function($http) {
    //
    //    //var urlBase = 'http://www.nmdad3.local/app_dev.php/api/v1/users/:id/reports';
    //    var urlBase = 'http://www.nmdad3.local/app_dev.php/api/v1/users/';
    //    var reportFactory = {};
    //
    //    reportFactory.getReports = function (id) {
    //        return $http.get(urlBase + '1' + '/reports');
    //    };
    //
    //    reportFactory.postReport = function (id, rep) {
    //        return $http.post(urlBase + '1' + '/reports', rep);
    //        //return $http.post(urlBase + id + '/reports', rep);
    //    };
    //
    //    //reportFactory.updateCustomer = function (cust) {
    //    //    return $http.put(urlBase + '/' + cust.ID, cust)
    //    //};
    //    //
    //    //reportFactory.deleteCustomer = function (id) {
    //    //    return $http.delete(urlBase + '/' + id);
    //    //};
    //
    //    return reportFactory;
    //}])

;


