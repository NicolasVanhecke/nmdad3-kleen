/**
 * Created by Nicolas on 07/11/15.
 */

$(document).ready(function() {
    console.log("started");
    // hide flashmessages after few seconds
    $(".flash-succes").delay(3500).fadeOut(1000);
    $(".flash-error").delay(3500).fadeOut(1000);
    // loading all functions
    bootstrapDataTable();
    setSidebarHeight();
    // Check if page needs to load GMaps
    $(function(){
        if($('#loadGMaps').is('.loadAllReports')){
            loadGMapsAll();
        } else if ($('#loadGMaps').is('.loadOneReport')){
            loadGMapsOne();
        }
    });
    console.log("all functions loaded");
});

// Also load size calculating functions on window resize
$(window).resize(function() {
    setSidebarHeight();
});


function bootstrapDataTable(){
    $('#datatable').DataTable({
        "pageLength" : 25
    });
}


function setSidebarHeight(){
    height = 0;

    // Get height from content
    $("#content").each(function(){
        if ($(this).height() > height)
        {
            // + 40 from 20 margin-top and 20 margin-bottom
            contentHeight = $(this).height() + 40;
        }
    });

    // Add height from content to sidebar margin as CSS style
    styles = {'height': (contentHeight+'px') };
    $("#sidebar").css(styles);

}


function loadGMapsAll() {

    // Create an array of styles.
    var styles = [
        {
            "featureType": "administrative.locality",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#2c2e33"
                },
                {
                    "saturation": 7
                },
                {
                    "lightness": 19
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#ffffff"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 100
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#ffffff"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 100
                },
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "hue": "#bbc0c4"
                },
                {
                    "saturation": -93
                },
                {
                    "lightness": 31
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
                {
                    "hue": "#bbc0c4"
                },
                {
                    "saturation": -93
                },
                {
                    "lightness": 31
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels",
            "stylers": [
                {
                    "hue": "#bbc0c4"
                },
                {
                    "saturation": -93
                },
                {
                    "lightness": -2
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "hue": "#e9ebed"
                },
                {
                    "saturation": -90
                },
                {
                    "lightness": -8
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#e9ebed"
                },
                {
                    "saturation": 10
                },
                {
                    "lightness": 69
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#90E1FF"
                },
                {
                    "saturation": 40
                },
                {
                    "lightness": 67
                },
                {
                    "visibility": "simplified"
                }
            ]
        }
    ];

    // Create a new StyledMapType object, passing it the array of styles,
    // as well as the name to be displayed on the map type control.
    var styledMap = new google.maps.StyledMapType(styles,
        {name: "Styled Map"});

    var data = JSON.parse($('#jsondata').text());

    // Make GMap in the Element with id map
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: {lat: 51.082527, lng: 3.6444458},    // Ghent
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
    });

    $(".panTo").on("click", function() {
        // Take variables from data attr in html
        var lat = $(this).data('lat');
        var long = $(this).data('long');

        // Set long and lat values for map to zoom to
        var clickedLatLng = new google.maps.LatLng( lat,  long);
        map.panTo(clickedLatLng);
        map.setZoom(16);
    });

    // Loop each data entry from the database and make a marker
    $.each(data, function(marker, data) {
        var latLng = new google.maps.LatLng(data.location_lat, data.location_long);

        //var state = $('tr').data('history');
        //console.log(state);
        //var stateString = state.toString();
        //console.log(stateString);

        // ^^ ToDo: check for last state and give correct image ^^
        // Setting up custom markers for each state of report
        //if (state === 'fake') {
        //    var image = 'http://www.nmdad3.local/images/fake.png';
        //} else if (state === 'in behandeling') {
        //    var image = 'http://www.nmdad3.local/images/inbehandeling.png';
        //} else if (state === 'opgeruimd') {
        //    var image = 'http://www.nmdad3.local/images/opgeruimd.png';
        //} else {
        //    var image = 'http://www.nmdad3.local/images/gemeld.png';
        //}
            var image = 'http://www.nmdad3.local/images/gemeld.png';

        // Creating a marker and putting it on the map
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            icon: image,
            title: data.description
        });

        var contentId = data.id;
        var contentDescription = data.description;
        var contentType = data.type;

        // Parse date to readable string
        var date = new Date(data.created_at);
        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var myDate = '';
        myDate += days[date.getDay()] + " " + months[date.getMonth()] + " " + date.getDate() + " " + date.getFullYear() + " ";
        myDate += date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

        var infowindow = new google.maps.InfoWindow({
            content: '<b>#</b> ' + contentId + '<br />'
                + '<b>Type: </b>' + contentType + '<br />'
                + '<b>Omschrijving: </b>' + contentDescription + '<br />'
                + '<b>Gemeld: </b>' + myDate
        });

        // Add clickevent: show details when clicked on individual marker
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

    });

}


function loadGMapsOne() {
    // Create an array of styles.
    var styles = [
        {
            "featureType": "administrative.locality",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#2c2e33"
                },
                {
                    "saturation": 7
                },
                {
                    "lightness": 19
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#ffffff"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 100
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#ffffff"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 100
                },
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "hue": "#bbc0c4"
                },
                {
                    "saturation": -93
                },
                {
                    "lightness": 31
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
                {
                    "hue": "#bbc0c4"
                },
                {
                    "saturation": -93
                },
                {
                    "lightness": 31
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels",
            "stylers": [
                {
                    "hue": "#bbc0c4"
                },
                {
                    "saturation": -93
                },
                {
                    "lightness": -2
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "hue": "#e9ebed"
                },
                {
                    "saturation": -90
                },
                {
                    "lightness": -8
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#e9ebed"
                },
                {
                    "saturation": 10
                },
                {
                    "lightness": 69
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#90E1FF"
                },
                {
                    "saturation": 40
                },
                {
                    "lightness": 67
                },
                {
                    "visibility": "simplified"
                }
            ]
        }
    ];

    // Create a new StyledMapType object, passing it the array of styles,
    // as well as the name to be displayed on the map type control.
    var styledMap = new google.maps.StyledMapType(styles,
        {name: "Styled Map"});

    var data = JSON.parse($('#jsondata').text());

    // Make only one marker with the long lat from the selected report
    var latLng = new google.maps.LatLng(data.location_lat, data.location_long);

    // Make GMap in the Element with id map
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Setting up custom markers for each state of report
    if (data.state == 'fake') {
        var image = 'http://www.nmdad3.local/images/fake.png';
    } else if (data.state == 'in behandeling') {
        var image = 'http://www.nmdad3.local/images/inbehandeling.png';
    } else if (data.state == 'opgeruimd') {
        var image = 'http://www.nmdad3.local/images/opgeruimd.png';
    } else {
        var image = 'http://www.nmdad3.local/images/gemeld.png';
    }

    // Creating a marker and putting it on the map
    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        icon: image,
        title: data.description
    });

    //var contentString = "'#' + data.id + data.description + 'from: ' + data.user.username";
    var contentId = data.id;
    var contentDescription = data.description;

    var infowindow = new google.maps.InfoWindow({
        content: '# ' + contentId + '<br />' + contentDescription
    });

    infowindow.open(map, marker);

    //Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
}
