<?php

namespace ApiBundle\Form;

use AppBundle\Entity\Leaderboard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LeaderboardType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', null, ['required' => false])
            ->add('userId')
            ->add('completed')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Leaderboard::class,
            'csrf_protection' => false,
        ]);
    }

    /**
     * JSON object name.
     *
     * { leaderboard: { ... } }
     *
     * @return string
     */
    public function getName()
    {
        return 'leaderboard';
    }
}
