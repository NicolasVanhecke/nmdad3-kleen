<?php

namespace ApiBundle\Form;

use AppBundle\Entity\Badge;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BadgeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', null, ['required' => false])
            ->add('title')
            ->add('body')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Badge::class,
            'csrf_protection' => false,
        ]);
    }

    /**
     * JSON object name.
     *
     * { report: { ... } }
     *
     * @return string
     */
    public function getName()
    {
        return 'report';
    }
}
