<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\UserType;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class UserRestController.
 *
 * @Route("/v1")
 */
class UserRestController extends FOSRestController
{
    // ToDo: If code doesn't work, try replacing "/users" for "/users/"
    /**
     * Test API options and requirements.
     *
     * @FOSRest\Options("/users")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function optionsAction()
    {
        # HTTP method: OPTIONS
        # Host/port  : http://www.nmdad3.local
        #
        # Path       : /app_dev.php/api/v1/users/

        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * Returns one user by id
     *
     * @param $user_id
     *
     * @return mixed
     * @FOSRest\Get("/users/{user_id}.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function getUserAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:User')
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. No user found with this id.');
        }

        return $user;
    }

    /**
     * Return all users.
     *
     * @return mixed
     * @FOSRest\Get("/users.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @FOSRest\QueryParam(name="sort", requirements="id|created_at", default="id", description="Order by users id or created date.")
     * @FOSRest\QueryParam(name="order", requirements="asc|desc", default="asc", description="Order result ascending or descending.")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function getUsersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:User')
            ->findAll();

        return $user;
    }

    /**
     * Create new user.
     *
     * @param Request $request
     *
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post("/users/.{_format}", requirements = { "user_id": "\d+", "_format" : "json|xml" }, defaults = {"_format": "json"})
     * @Nelmio\ApiDoc(
     *     input = UserType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = new User();
//        $report->setUser($user);

        $logger = $this->get('logger');
        $logger->info($request);

        return $this->processUserForm($request, $user);
    }

    /**
     * Update a user.
     *
     * @param Request $request
     * @param $user_id
     *
     * @return Response
     *
     * @FOSRest\View()
     * @FOSRest\Put("/users/{user_id}.{_format}", requirements = {"user_id" : "\d+", "$report_id" : "\d+", "_format" : "json|xml"})
     * @Nelmio\ApiDoc(
     *     input = UserType::class,
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content"
     *     }
     * )
     */
    public function putUserAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:Report')
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Oops, no user found with this id. Please try again.');
        }

        return $this->processUserForm($request, $user);
    }

    /**
     * Delete a user.
     *
     * @param $user_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete("/users/{user_id}.{_format}", requirements = { "user_id": "\d+", "_format": "json|xml" }, defaults = {"_format": "json"})
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteUserAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em
            ->getRepository('AppBundle:User')
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('No user found for this id.');
        }

        $em->remove($user);
        $em->flush();
    }


    // Convenience methods
    // -------------------

    /**
     * Process UserType Form.
     *
     * @param Request $request
     * @param User $user
     *
     * @return View|Response
     */
    private function processUserForm(Request $request, User $user)
    {
        $form = $this->createForm(new UserType(), $user, ['method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($user->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);  // Manage entity Article for persistence.
            $em->flush();           // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('api_v1_get_user', [
                    'user_id' => $user->getUser()->getId(),
                ], /* absolute path = */true)
            );

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
