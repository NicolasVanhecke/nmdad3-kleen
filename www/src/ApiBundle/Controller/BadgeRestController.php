<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\BadgeType;
use AppBundle\Entity\Badge;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class BadgeRestController.
 *
 * @Route("/v1")
 */
class BadgeRestController extends FOSRestController
{
    /**
     * Test API options and requirements.
     *
     * @FOSRest\Options("/users/{user_id}/badges")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function optionsAction()
    {
        # HTTP method: OPTIONS
        # Host/port  : http://www.nmdad3.local
        #
        # Path       : /app_dev.php/api/v1/users/1/reports/

        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * Returns all badges per user id.
     *
     * @param $user_id
     * @param ParamFetcher $paramFetcher
     *
     * @return mixed
     * @FOSRest\Get("/users/{user_id}/badges.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @FOSRest\QueryParam(name="sort", requirements="id|title", default="id", description="Order by reports id or reports title.")
     * @FOSRest\QueryParam(name="order", requirements="asc|desc", default="asc", description="Order result ascending or descending.")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getUserBadgesAction($user_id, ParamFetcher $paramFetcher)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:User')
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. No user found with this id.');
        }

        $badges = $user->getBadges();

        return $badges;
    }

    /**
     * Returns one badge by id.
     *
     * @param $id
     *
     * @return mixed
     * @FOSRest\Get("/badges/{id}.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @FOSRest\QueryParam(name="sort", requirements="id|title", default="id", description="Order by reports id or reports title.")
     * @FOSRest\QueryParam(name="order", requirements="asc|desc", default="asc", description="Order result ascending or descending.")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getBadgeAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $badge = $em
            ->getRepository('AppBundle:Badge')
            ->find($id);

        if (!$badge instanceof Badge) {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. No user found with this id.');
        }

        return $badge;
    }

    /**
     * Returns all badges.
     *
     * @return mixed
     * @FOSRest\Get("/badges.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @FOSRest\QueryParam(name="sort", requirements="id|title", default="id", description="Order by reports id or reports title.")
     * @FOSRest\QueryParam(name="order", requirements="asc|desc", default="asc", description="Order result ascending or descending.")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getBadgesAction()
    {

        $em = $this->getDoctrine()->getManager();
        $badges = $em
            ->getRepository('AppBundle:Badge')
            ->findAll();

        return $badges;
    }


    /**
     * Delete a badge.
     *
     * @param $badge_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete("/badges/{badge_id}.{_format}", requirements = { "user_id": "\d+", "_format": "json|xml" }, defaults = {"_format": "json"})
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteBadgeAction($badge_id)
    {
        $em = $this->getDoctrine()->getManager();

        $badge = $em
            ->getRepository('AppBundle:Badge')
            ->find($badge_id);

        if (!$badge instanceof Badge) {
            throw new NotFoundHttpException('No badge found for this id.');
        }

        $em->remove($badge);
        $em->flush();
    }


}
