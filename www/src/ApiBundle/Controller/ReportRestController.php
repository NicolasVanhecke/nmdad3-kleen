<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\ReportType;
use AppBundle\Entity\Report;
use AppBundle\Entity\ReportState;
use AppBundle\Entity\User;
use Behat\Mink\Exception\Exception;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class ReportRestController.
 *
 * @Route("/v1")
 */
class ReportRestController extends FOSRestController
{
    /**
     * Test API options and requirements.
     *
     * @FOSRest\Options("/users/{user_id}/reports")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function optionsAction()
    {
        # HTTP method: OPTIONS
        # Host/port  : http://www.nmdad3.local
        #
        # Path       : /app_dev.php/api/v1/users/1/reports/

        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * Returns all reports.
     *
     *
     * @return mixed
     * @FOSRest\Get("/reports.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @FOSRest\QueryParam(
     *     name="sort",
     *     requirements="id|createdAt",
     *     default="id",
     *     description="Order by reports id or createdAt.")
     * @FOSRest\QueryParam(
     *     name="order",
     *     requirements="asc|desc",
     *     default="asc",
     *     description="Order result ascending or descending.")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getReportsAction()
    {

        $em = $this->getDoctrine()->getManager();
        $reports = $em
            ->getRepository('AppBundle:Report')
            ->findAll();

        return $reports;
    }

    /**
     * Returns all reports for user.
     *
     * @param $user_id
     *
     * @return mixed
     * @FOSRest\Get("/users/{user_id}/reports.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @FOSRest\QueryParam(name="sort", requirements="id|createdAt", default="id", description="Order by reports id or reports createdAt.")
     * @FOSRest\QueryParam(name="order", requirements="asc|desc", default="asc", description="Order result ascending or descending.")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getUserReportAction($user_id, ParamFetcher $paramFetcher)

    {

        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:User')
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. No user found with this id.');
        }

        $reports = $user->getReports();

//        var_dump($reports);
        return $reports;
    }

    /**
     * Returns one report.
     *
     * @param $user_id
     * @param $report_id
     *
     * @return mixed
     * @FOSRest\Get("/users/{user_id}/reports/{report_id}.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @FOSRest\QueryParam(name="sort", requirements="id|createdAt", default="id", description="Order by reports id or reports createdAt.")
     * @FOSRest\QueryParam(name="order", requirements="asc|desc", default="asc", description="Order result ascending or descending.")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getReportAction($user_id, $report_id)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $em
            ->getRepository('AppBundle:User')
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. No user found with this id.');
        }

        $report = $em
            ->getRepository('AppBundle:Report')
            ->findBy(
                array('id' => $report_id),     // criteria
                array('createdAt' => 'ASC') // order
            );

        return $report;
    }

    /**
     * Returns state history for report.
     *
     * @param $user_id
     * @param $report_id
     *
     * @return mixed
     * @FOSRest\Get("/users/{user_id}/reports/{report_id}/reportstates.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @FOSRest\QueryParam(name="sort", requirements="id|createdAt", default="id", description="Order by reports id or reports createdAt.")
     * @FOSRest\QueryParam(name="order", requirements="asc|desc", default="asc", description="Order result ascending or descending.")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getReportStatesAction($user_id, $report_id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:User')
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. No user found with this id.');
        }

        $report = $em
            ->getRepository('AppBundle:Report')
            ->findOneBy(
                array('id' => $report_id),   // criteria
                array('createdAt' => 'ASC')     // order
            );

        if (!$report instanceof Report) {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. No user found with this id.');
        }

        $reportstates = $em
            ->getRepository('AppBundle:ReportState')
            ->findBy(
                array('report' => $report),     // criteria
                array('createdAt' => 'DESC') // order
            );

        return $reportstates;
    }

    /**
     * Post new report.
     * {report:{...}}
     * @param Request $request
     * @param $user_id
     *
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post("/users/{user_id}/reports", requirements = { "user_id": "\d+", "_format" : "json|xml" }, defaults = {"_format": "json"})
     * @Nelmio\ApiDoc(
     *     input = ReportType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postReportAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. No user found for with this id.');
        }

        $report = new Report();
        $report->setUser($user);

        $logger = $this->get('logger');
        $logger->info($request);

//        $response = new Response();
//        $response->setContent(json_encode([$request]));
//        return $response;

        return $this->processReportForm($request, $report);
    }

    /**
     * Update a report.
     *
     * @param Request $request
     * @param $user_id
     * @param $report_id
     *
     * @return Response
     *
     * @FOSRest\View()
     * @FOSRest\Put("/users/{user_id}/reports/{report_id}.{_format}", requirements = {"user_id" : "\d+", "$report_id" : "\d+", "_format" : "json|xml"})
     * @Nelmio\ApiDoc(
     *     input = ReportType::class,
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content"
     *     }
     * )
     */
    public function putArticleAction(Request $request, $user_id, $report_id)
    {
        $em = $this->getDoctrine()->getManager();
        $report = $em
            ->getRepository('AppBundle:Report')
            ->find($report_id);

        if (!$report instanceof Report) {
            throw new NotFoundHttpException('No report found for this id. Please try again.');
        }

        if ($report->getUser()->getId() === (int) $user_id) {
            return $this->processReportForm($request, $report);
        } else {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. Please try again.');
        }
    }

    /**
     * Delete a report.
     *
     * @param $user_id
     * @param $report_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete("/users/{user_id}/reports/{report_id}.{_format}", requirements = { "user_id"   : "\d+", "report_id" : "\d+", "_format"   : "json|xml" }, defaults = {"_format": "json"})
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteArticleAction($user_id, $report_id)
    {
        $em = $this->getDoctrine()->getManager();

        $report = $em
            ->getRepository('AppBundle:Report')
            ->find($report_id);

        if (!$report instanceof Report) {
            throw new NotFoundHttpException('No report found for this id');
        }

        if ($report->getUser()->getId() === (int) $user_id) {
            $em->remove($report);
            $em->flush();
        } else {
            throw new NotFoundHttpException('Oh no, something went horribly wrong. Please try again.');
        }
    }

    // Convenience methods
    // -------------------

    /**
     * Process ReportyType Form.
     *
     * @param Request $request
     * @param Report $report
     *
     * @return View|Response
     */
    private function processReportForm(Request $request, Report $report)
    {
        $form = $this->createForm(new ReportType(), $report, ['method' => $request->getMethod()]);
        $form->handleRequest($request);
        // $form->submit();

       if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($report);  // Manage entity Report for persistence.
            $em->flush();           // Persist to database.

            $statusCode = is_null($report->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('api_v1_get_user_report', [
                    'user_id' => $report->getUser()->getId(),
                    'report_id' => $report->getId(),
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                'report' => ['id' => $report->getId()],
            ]));

//           return $response;
            return $form->getErrors(true);
        }
        else {
            return $form->getErrors(true);
        }
//        return View::create($form->getErrors(true), Response::HTTP_BAD_REQUEST);
//        return View::create($form, Response::HTTP_BAD_REQUEST);
    }


}
