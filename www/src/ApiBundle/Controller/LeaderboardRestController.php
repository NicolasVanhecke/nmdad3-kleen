<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\LeaderboardType;
use AppBundle\Entity\Leaderboard;
use AppBundle\Entity\User;
use Behat\Mink\Exception\Exception;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class LeaderboardRestController.
 *
 * @Route("/v1")
 */
class LeaderboardRestController extends FOSRestController
{
    /**
     * Test API options and requirements.
     *
     * @FOSRest\Options("/leaderboards")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function optionsAction()
    {
        # HTTP method: OPTIONS
        # Host/port  : http://www.nmdad3.local
        #
        # Path       : /app_dev.php/api/v1/users/1/reports/

        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * Returns leaderboard.
     *
     *
     * @return mixed
     * @FOSRest\Get("/leaderboards.{_format}", requirements = { "_format": "json|jsonp|xml" }, defaults = {"_format": "json"})
     * @FOSRest\QueryParam(name="sort", requirements="id|createdAt", default="id", description="Order by reports id or reports title.")
     * @FOSRest\QueryParam(name="order", requirements="asc|desc", default="asc", description="Order result ascending or descending.")
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getLeaderboardAction()
    {

        $em = $this->getDoctrine()->getManager();
        $leaderboard = $em
            ->getRepository('AppBundle:Leaderboard')
            ->findBy(
                array(), // criteria
                array('completed' => 'DESC'), // order
                10, // limit
                0 // offset
            );

        return $leaderboard;
    }


}
