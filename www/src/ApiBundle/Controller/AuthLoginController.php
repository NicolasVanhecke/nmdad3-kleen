<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\AuthType;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class AuthLoginController.
 *
 * @Route("/v1")
 */
class AuthLoginController extends FOSRestController
{
    /**
     * Test API options and requirements.
     *
     * @FOSRest\Options("/auth/login")
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function optionsAction()
    {
        # HTTP method: OPTIONS
        # Host/port  : http://www.nmdad3.local
        #
        # Path       : /app_dev.php/api/v1/auth/login

        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS');

        return $response;
    }
    /**
     * Auth login post.
     *
     * @param Request $request
     * @param $user
     *
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post("/auth/login")
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function authLoginAction(Request $request)
    {
        //gebruiker ophalen op basis van de headers in je request
        $base64 = $request->headers->get("Authorization");

        // Authorization op te halen
        //base 64 decoderen (gebruikersnaam:password
        //gebruiker ophalen uit databank op basis van gebruikersnaam
        //wachtwoord vergelijken door eerst het meegegeven ww te hashen

        //gebruiker is ingelogd

        $response = new Response();

        // Redirect to the URI of the resource.
        $response->headers->set('Location',
            $this->generateUrl('api_v1_get_user_auth', [
                'user_id' => $user->getId()
            ], /* absolute path = */true)
        );

        return $response;
    }





}
