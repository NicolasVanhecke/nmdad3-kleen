<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
//    public function __construct() {
//    }
//
//
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome on the homepage', $crawler->filter('h1')->text());
    }
//
//
//    public function testGetUserReturnsUserWithExpectedValues()
//    {
//        $details = array();
//
//        $user = new User($details);
//        $password = 'fubar';
//
//        $user->setPassword($password);
//    }
//
//
//    public function testCreateUser()
//    {
//        // Create a new client to browse the application
//        $client = static::createClient();
//
//        // Create a new entry in the database
//        $crawler = $client->request('GET', '/users/');
//        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /users/");
//        $crawler = $client->click($crawler->selectLink('Maak nieuwe gebruiker aan')->link());
//
//        // Fill in the form and submit it
//        $form = $crawler->selectButton('Maak gebruiker aan')->form(array(
//            'appbundle_user_new[firstName]'  => 'Maxim',
//            'appbundle_user_new[lastName]'  => 'Verbeke',
//            'appbundle_user_new[birthday]'  => '01-01-2016',
//            'appbundle_user_new[email]'  => 'verbekemaxim@me.com',
//            'appbundle_user_new[username]'  => 'verbekem',
//            'appbundle_user_new[passwordRaw][first]'  => 'verbekem',
//            'appbundle_user_new[passwordRaw][second]'  => 'verbekem',
//        ));
//
//        $client->submit($form);
//
////        $crawler = $client->followRedirect();
//        $crawler = $client->request('GET', '/users/');
//        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /users/");
//
//        // Check if data shows up in redirect view
//        $this->assertGreaterThan(0, $crawler->filter('td:contains("verbekemaxim@me.com")')->count(), 'Missing element td:contains("verbekemaxim@me.com")');
//    }
//
//
//    public function testEditUser()
//    {
//        // Create a new client to browse the application
//        $client = static::createClient();
//
//        $crawler = $client->request('GET', '/users/1');
//        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /users/");
//
//        // Edit the entity
//        $crawler = $client->click($crawler->selectLink('Wijzig gegevens')->link());
//
//        $form = $crawler->selectButton('Opslaan')->form(array(
//            'appbundle_user[firstName]'  => 'Nico',
//            'appbundle_user[lastName]'  => 'Vanhecke',
//            'appbundle_user[birthday]'  => '01-01-2016',
//            'appbundle_user[email]'  => 'vanhecke.nicolas@hotmail.com',
//            'appbundle_user[username]'  => 'NicolasV',
//            'appbundle_user[password]'  => 'nicolasvh',
//        ));
//
//        $client->submit($form);
//        $crawler = $client->followRedirect();
//
//        $this->assertContains('Nico', $crawler->filter('h2')->text());
//
//    }

//    public function testMockUpUser()
//    {
//        // Create a new client to browse the application
//        $client = static::createClient();
//
//        $crawler = $client->request('GET', '/users/');
//        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /users/");
//
//        // First, mock the object to be used in the test
//        $user = $this->getMock(User::class);
//
//        // Second, mock the repository so it returns the mock of the employee
//        $userRepository = $this
//            ->getMockBuilder(EntityRepository::class)
//            ->disableOriginalConstructor()
//            ->getMock();
//        $userRepository->expects($this->once())
//            ->method('find')
//            ->will($this->returnValue($user));
//
//        // Last, mock the EntityManager to return the mock of the repository
//        $entityManager = $this
//            ->getMockBuilder(ObjectManager::class)
//            ->disableOriginalConstructor()
//            ->getMock();
//        $entityManager->expects($this->once())
//            ->method('getRepository')
//            ->will($this->returnValue($userRepository));
//
//        $userGenerator = new User($entityManager);
//        $userGenerator->setFirstName('Xaviertje');
//        $this->assertEquals('Xaviertje', $userGenerator->getFirstName());   // User is now successfully created
//    }

}
