<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Report.
 *
 * @ORM\Table(name="reports")
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @JMS\ExclusionPolicy("all")
 */
class Report
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     * @JMS\Expose()
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     * @JMS\Expose()
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="location_lat", type="decimal", scale=6)
     * @JMS\Expose()
     */
    private $locationLat;

    /**
     * @var string
     *
     * @ORM\Column(name="location_long", type="decimal", scale=6)
     * @JMS\Expose()
     */
    private $locationLong;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=65536)
     * @JMS\Expose()
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="laststate", type="string", length=255, nullable=true)
     * @JMS\Expose()
     */
    private $laststate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @JMS\Expose()
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="completed_at", type="datetime")
     */
    private $completedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime")
     */
    private $deletedAt;



    // Relationships
    /**
     * @var User
     *
     * @Assert\NotBlank()
     *
     * Many-to-one relationship with User inversed by User::$reports.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reports")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @var ArrayCollection
     *
     * One-to-many relationship to ReportState mapped by ReportState::$report
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#many-to-many-bidirectional
     *
     * @ORM\OneToMany(targetEntity="ReportState", mappedBy="report")
     * @JMS\Exclude()
     */
    private $reportstates;




    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
        $this->setCompletedAt(new \DateTime());
        $this->setDeletedAt(new \DateTime());
        $this->setReportStates(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Report
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Report
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set locationLat
     *
     * @param string $locationLat
     *
     * @return Report
     */
    public function setLocationLat($locationLat)
    {
        $this->locationLat = $locationLat;

        return $this;
    }

    /**
     * Get locationLat
     *
     * @return string
     */
    public function getLocationLat()
    {
        return $this->locationLat;
    }

    /**
     * Set locationLong
     *
     * @param string $locationLong
     *
     * @return Report
     */
    public function setLocationLong($locationLong)
    {
        $this->locationLong = $locationLong;

        return $this;
    }

    /**
     * Get locationLong
     *
     * @return string
     */
    public function getLocationLong()
    {
        return $this->locationLong;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Report
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Report
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Report
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set completedAt
     *
     * @param \DateTime $completedAt
     *
     * @return Report
     */
    public function setCompletedAt($completedAt)
    {
        $this->completedAt = $completedAt;

        return $this;
    }

    /**
     * Get completedAt
     *
     * @return \DateTime
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Report
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set laststate
     *
     * @param string $laststate
     *
     * @return Report
     */
    public function setLastState($laststate)
    {
        $this->lastState = $laststate;

        return $this;
    }

    /**
     * Get laststate
     *
     * @return string
     */
    public function getLastState()
    {
        return $this->laststate;
    }


    // Methods for Relationships.
    // --------------------------

    /**
     * Set user.
     *
     * @param $user
     *
     * @return Report
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set reportstates.
     *
     * @param ArrayCollection $reportstates
     *
     * @return Report
     */
    public function setReportStates(ArrayCollection $reportstates)
    {
        $this->reportstates = $reportstates;

        return $this;
    }

    /**
     * Get reportstates.
     *
     * @return ReportState
     */
    public function getReportStates()
    {
        return $this->reportstates;
    }



    /**
     * Add reportstate
     *
     * @param \AppBundle\Entity\ReportState $reportstate
     *
     * @return Report
     */
    public function addReportstate(\AppBundle\Entity\ReportState $reportstate)
    {
        $this->reportstates[] = $reportstate;

        return $this;
    }

    /**
     * Remove reportstate
     *
     * @param \AppBundle\Entity\ReportState $reportstate
     */
    public function removeReportstate(\AppBundle\Entity\ReportState $reportstate)
    {
        $this->reportstates->removeElement($reportstate);
    }
}
