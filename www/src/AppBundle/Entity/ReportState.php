<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ReportState
 *
 * @ORM\Table(name="reportstates")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ReportStateRepository")
 * @ORM\InheritanceType("JOINED")
 * @JMS\ExclusionPolicy("all")
 */
class ReportState
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose()
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="string")
     * @JMS\Expose()
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="string")
     * @JMS\Expose()
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @JMS\Expose()
     */
    private $createdAt;



    // Relationships
    /**
     * @var Report
     *
     * @Assert\NotBlank()
     *
     * Many-to-one relationship with Report inversed by Report::$reportstates.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @ORM\ManyToOne(targetEntity="Report", inversedBy="reportstates")
     * @ORM\JoinColumn(name="report", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @JMS\Expose()
     */
    protected $report;



    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setReport(new ArrayCollection());
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return ReportState
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }


    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    // Methods for Relationships.
    // --------------------------

//    /**
//     * Set reports.
//     *
//     * @param ArrayCollection $reports
//     *
//     * @return State
//     */
//    public function setReports(ArrayCollection $reports)
//    {
//        $this->reports = $reports;
//
//        return $this;
//    }
//
//    /**
//     * Get reports.
//     *
//     * @return Report
//     */
//    public function getReports()
//    {
//        return $this->reports;
//    }

    /**
     * Set user_id
     *
     * @param integer $user_id
     *
     * @return ReportState
     */
    public function setUserId($user_id)
    {
        $this->userId = $user_id;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }


    /**
     * Set report
     *
     * @param $report
     *
     * @return ReportState
     */
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }
}
