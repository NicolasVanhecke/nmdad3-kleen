<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Badge
 *
 * @ORM\Table(name="badges")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\BadgeRepository")
 */
class Badge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="reports", type="decimal")
     */
    private $reports;



    // Relationships
//    /**
//     * @var User
//     *
//     * @Assert\NotBlank()
//     *
//     * Many-to-one relationship with Badge inversed by User::$badges.
//     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
//     *
//     * @ORM\ManyToOne(targetEntity="User", inversedBy="badges")
//     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
//     * @JMS\Exclude()
//     */
//    protected $user;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Badge
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set reports
     *
     * @param string $reports
     *
     * @return Badge
     */
    public function setReports($reports)
    {
        $this->reports = $reports;

        return $this;
    }

    /**
     * Get reports
     *
     * @return string
     */
    public function getReports()
    {
        return $this->reports;
    }


    // Methods for Relationships.
    // --------------------------

//    /**
//     * Set user.
//     *
//     * @param $user
//     *
//     * @return Badge
//     */
//    public function setUser($user)
//    {
//        $this->user = $user;
//
//        return $this;
//    }
//
//    /**
//     * Get user.
//     *
//     * @return User
//     */
//    public function getUser()
//    {
//        return $this->user;
//    }

}

