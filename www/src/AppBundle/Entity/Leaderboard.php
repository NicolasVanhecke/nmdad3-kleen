<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Leaderboard
 *
 * @ORM\Table(name="leaderboards")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\LeaderboardRepository")
 * @ORM\InheritanceType("JOINED")
 * @JMS\ExclusionPolicy("all")
 */
class Leaderboard
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose()
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="completed", type="integer")
     * @JMS\Expose()
     */
    private $completed;




    // Relationships
    /**
     * @var User
     *
     * @Assert\NotBlank()
     *
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @JMS\Expose()
     */
    protected $user;






    /**
     * Constructor.
     */
    public function __construct()
    {
//        $this->setUsers(new ArrayCollection());
        $this->completed=0;
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Leaderboard
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set completed
     *
     * @param string $completed
     *
     * @return Leaderboard
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    public function addPoints(){
        $this->completed++;
    }

    /**
     * Get completed
     *
     * @return string
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    // Methods for Relationships.
    // --------------------------

//    /**
//     * Set users.
//     *
//     * @param ArrayCollection $users
//     *
//     * @return Leaderboard
//     */
//    public function setUsers(ArrayCollection $users)
//    {
//        $this->user = $users;
//
//        return $this;
//    }
//
//    /**
//     * Get $users.
//     *
//     * @return User
//     */
//    public function getUsers()
//    {
//        return $this->user;
//    }

}
