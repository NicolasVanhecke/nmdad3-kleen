<?php

namespace AppBundle\Entity;

/**
 * LeaderboardRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LeaderboardRepository extends \Doctrine\ORM\EntityRepository
{
//    /**
//     * Overload findAll().
//     *
//     * @return array
//     */
//    public function findAll()
//    {
//        return $this->findBy([], ['createdAt' => 'DESC', 'id' => 'DESC']);
//    }

    public function findByUser($user){
        $qb = $this->_em->createQueryBuilder();
        $qb->select(array('l'))
            ->from('AppBundle:Leaderboard','l')
            ->join('AppBundle:User','u')
            ->where($qb->expr()->eq('u.id',$user->getId()));

        return $qb->getQuery()->getSingleResult();

    }

}
