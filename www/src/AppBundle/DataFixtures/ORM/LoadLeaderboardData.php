<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Leaderboard;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadLeaderboardData.
 *
 */
class LoadLeaderboardData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 5; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $leaderboard = new Leaderboard();
        $em->persist($leaderboard); // Manage Entity for persistence.
        $leaderboard
            ->setCompleted(22)
            ->setUser($this->getReference('NicolasV')); // Get reference from a previous Data Fixture.

        // Load reports with state "gemeld"
        for ($i = 1; $i < 10; ++$i) {
            $leaderboard = new Leaderboard();
            $em->persist($leaderboard); // Manage Entity for persistence.
            $leaderboard
                ->setCompleted($faker->numberBetween($min = 1, $max = 37))
                ->setUser($this->getReference("TestUser-${i}")); // Get reference from a previous Data Fixture.
        }

        $em->flush(); // Persist all managed Entities.
    }
}
