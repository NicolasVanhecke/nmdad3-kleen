<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ReportState;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadReportStateData.
 *
 */
class LoadReportStateData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 4; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

//        $reportstate = new ReportState();
//        $em->persist($reportstate); // Manage Entity for persistence.
//        $reportstate
//            ->setState('opgeruimd')
//            ->setUserId(1)
//            ->setReport($this->getReference('NicolasV')); // Get reference from a previous Data Fixture.

        $reportstate = new ReportState();
        $em->persist($reportstate); // Manage Entity for persistence.
        $reportstate
            ->setState('opgeruimd')
            ->setUserId(1)
            ->setReport($this->getReference('NicolasReport-2')); // Get reference from a previous Data Fixture.

        $reportstate = new ReportState();
        $em->persist($reportstate); // Manage Entity for persistence.
        $reportstate
            ->setState('opgeruimd')
            ->setUserId(1)
            ->setReport($this->getReference('NicolasReport-3')); // Get reference from a previous Data Fixture.

        $reportstate = new ReportState();
        $em->persist($reportstate); // Manage Entity for persistence.
        $reportstate
            ->setState('opgeruimd')
            ->setUserId(1)
            ->setReport($this->getReference('NicolasReport-4')); // Get reference from a previous Data Fixture.

        // Load reports with state "gemeld"
        for ($i = 1; $i < 4; ++$i) {
            $reportstate = new ReportState();
            $em->persist($reportstate); // Manage Entity for persistence.
            $reportstate
                ->setState('gemeld')
                ->setUserId($faker->numberBetween($min = 1, $max = 20))
                ->setReport($this->getReference("TestReport-${i}")); // Get reference from a previous Data Fixture.
        }

        // Load reports with state "in behandeling"
        for ($i = 5; $i < 9; ++$i) {
            $reportstate = new ReportState();
            $em->persist($reportstate); // Manage Entity for persistence.
            $reportstate
                ->setState('in behandeling')
                ->setUserId($faker->numberBetween($min = 1, $max = 20))
                ->setReport($this->getReference("TestReport-${i}")); // Get reference from a previous Data Fixture.
        }

        // Load reports with state "fake"
        for ($i = 10; $i < 12; ++$i) {
            $reportstate = new ReportState();
            $em->persist($reportstate); // Manage Entity for persistence.
            $reportstate
                ->setState('fake')
                ->setUserId($faker->numberBetween($min = 1, $max = 20))
                ->setReport($this->getReference("TestReport-${i}")); // Get reference from a previous Data Fixture.
        }

        // Load reports with state "opgeruimd"
        for ($i = 13; $i < 25; ++$i) {
            $reportstate = new ReportState();
            $em->persist($reportstate); // Manage Entity for persistence.
            $reportstate
                ->setState('opgeruimd')
                ->setUserId($faker->numberBetween($min = 1, $max = 20))
                ->setReport($this->getReference("TestReport-${i}")); // Get reference from a previous Data Fixture.
        }

        $em->flush(); // Persist all managed Entities.
    }
}
