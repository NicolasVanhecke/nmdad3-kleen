<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Badge;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadBadgeData.
 *
 */
class LoadBadgeData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 3; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $badge = new Badge();
        $em->persist($badge); // Manage Entity for persistence.
        $badge
            ->setName('Veteran')
            ->setReports('100');

        $badge = new Badge();
        $em->persist($badge); // Manage Entity for persistence.
        $badge
            ->setName('Prestige')
            ->setReports('50');

        $badge = new Badge();
        $em->persist($badge); // Manage Entity for persistence.
        $badge
            ->setName('Amateur')
            ->setReports('20');

        $badge = new Badge();
        $em->persist($badge); // Manage Entity for persistence.
        $badge
            ->setName('Getting there')
            ->setReports('5');

        $badge = new Badge();
        $em->persist($badge); // Manage Entity for persistence.
        $badge
            ->setName('Newbie')
            ->setReports('1');

//        for ($i = 0; $i < 5; ++$i) {
//            $badge = new Badge();
//            $em->persist($badge); // Manage Entity for persistence.
//            $badge
//                ->setName($faker->word)
//                ->setReports($faker->randomDigit);
//        }

        $em->flush(); // Persist all managed Entities.
    }
}
