# language: en
Feature: Login Form
  In order to login
  As a user/admin
  I need to fill in the login form

  Background:
    Given the users are:
      |username   |password   |
      |nicolasv   |nicolasv   |
      |usern      |passw      |



  @mink:default
  Scenario: Go to login page, see if form loads correctly
    Given I am on "/security/login"
    And print current URL
    Then I should see "login"



  @mink:default
  Scenario Outline: Try to log in with invalid credentials
    Given I am on "/security/login"
    Then I should see "login"
    When I fill in "appbundle_security_login[username]" with "<username>"
    And I fill in "appbundle_security_login[password]" with "<password>"
    And I press "Login"
    Then I should see "<message>"
    Then I should be on "/security/login"

    Examples:
      |username   |password   | message          |
      |xxxxxxxxx  |xxxxxxxxx  | Bad credentials. |



  @mink:default
  Scenario Outline: Try to log in with valid credentials and log back out
    Given I am on "/security/login"
    Then I should see "login"
    When I fill in "appbundle_security_login[username]" with "<username>"
    And I fill in "appbundle_security_login[password]" with "<password>"
    And I press "Login"
    Then I should be on "/dash/"
    And I should see "Dashboard"
    And I should see text matching "Afmelden"
    When I follow "Afmelden"
    Then I should be on "/security/login"
    And I should see "login"

    Examples:
      |username     |password  |
      |nicolasvh    |nicolasvh |



  @mink:default
  Scenario Outline: Try to log in with valid credentials and log back out
    Given I am on "/security/login"
    Then I should see "login"
    When I fill in "appbundle_security_login[username]" with "<username>"
    And I fill in "appbundle_security_login[password]" with "<password>"
    And I press "Login"
    Then I should be on "/dash/"
    And I should see "Dashboard"
    And I should see "Gebruikers"
    When I follow "Gebruikers"
    Then I should be on "/users/"
    And I should see text matching "Gebruikersnaam"
    And I should see text matching "nicolasvh"

    Examples:
      |username     |password  |
      |nicolasvh    |nicolasvh |



#  @mink:default
#  Scenario Outline: Try to log in with valid credentials and log back out
#    Given I am on "/reports/1"
#    Then I should see text matching "Melding"
#    When I press "Toon alle meldingen"
#    Then I should be on "/reports"
#    And I should see text matching "Meldingen"
#    And I should see text matching "Afmelden"
#    When I follow "Afmelden"
#    Then I should be on "/security/login"
#    And I should see "login"
#
#    Examples:
#      |username     |password  |
#      |nicolasvh    |nicolasvh |



## De reeds voorgedefineerde stappen in de context (MinkContext) vind je met:
## vagrant@homestead$ bin/behat -di
## vagrant@homestead$ bin/behat -dl
