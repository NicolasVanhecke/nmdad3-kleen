<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Leaderboard;
use AppBundle\Entity\ReportState;
use Faker\Provider\tr_TR\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Report;

/**
 * User controller.
 *
 * @Route("/reports")
 */
class ReportController extends Controller
{
    /**
     * @Route("/", name="reports")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $reports = $em->getRepository('AppBundle:Report')->findAll();

        $completedReports = $em->getRepository('AppBundle:Report')->findBy(
            array('laststate' => 'opgeruimd'),  // requirement
            array('createdAt' => 'DESC')        // order
        );

        $uncompletedReports = $em->getRepository('AppBundle:Report')->findBy(
            array('laststate' => array('gemeld', 'in behandeling', 'fake')),    // requirement
            array('createdAt' => 'DESC')                                        // order
        );

        $serializer = $this->container->get('serializer');
        $reportsJSON = $serializer->serialize($reports, 'json');

        $users = $em->getRepository('AppBundle:User')->findAll();

        return array(
            'loggedUser'            => $loggedUser,
            'completedReports'      => $completedReports,
            'uncompletedReports'    => $uncompletedReports,
            'reports'               => $reports,
            'reportsJSON'           => $reportsJSON,
            'users'                 => $users,
        );
    }

    /**
     * Change status from Report.
     *
     * @Route("/update/{status}/{id}", name="report_status")
     * @Method("GET")
     * @Template()
     */
    public function updateAction($id, $status)
    {
        $em = $this->getDoctrine()->getManager();

        $report = $em->getRepository('AppBundle:Report')->find($id);

        $user = $em->getRepository('AppBundle:User')->findOneBy(
            array('id' => $report->getUser()));

        $update = new ReportState();
        $update ->setState($status)
                ->setUserId($user->getId())
                ->setCreatedAt(new \DateTime)
                ->setReport($report);
        $em->persist($update);

        $report->addReportstate($update);
        $em->flush();

        $message = 'Report ' . $id . ' is nu "' . $status . '"';
        $this->addFlash('succes', $message);

        return $this->redirectToRoute('reports');
    }

    /**
     * Complete report status.
     *
     * @Route("/complete/{status}/{id}", name="report_complete")
     * @Method("GET")
     * @Template()
     */
    public function completeAction($id, $status)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $report = $em->getRepository('AppBundle:Report')->find($id);

        $user = $em->getRepository('AppBundle:User')->findOneBy(
            array('id' => $report->getUser()));

//        $user = $em->getRepository('AppBundle:User')->findBy(
//            array('id' => $report->getUser()));

        $lb = $em->getRepository('AppBundle:Leaderboard')->findBy(
            array('user' => $user));

        $update = new ReportState();
        $update ->setState($status)
                ->setUserId($user->getId())
                ->setCreatedAt(new \DateTime)
                ->setReport($report);
        $em->persist($update);

        $report->addReportstate($update);
        $report->setLastState($status);

        if ($status == 'opgeruimd') {
            // if user isn't in leaderboards yet make new entry
            // if user already has a score in leaderboard, call addPoint() to update score by 1
            if (!$lb) {
                $new = new Leaderboard();
                $new->setUser($user);
                $new->addPoints();
                $em->persist($new);
            } else {
                $update = $em->getRepository('AppBundle:Leaderboard')->findOneBy(array('user' => $user));
                $update->addPoints();
                $em->merge($update);
            }
        }


        $em->flush();

        $message = 'Report ' . $id . ' is nu "' . $status . '"';
        $this->addFlash('succes', $message);

        return $this->redirectToRoute('reports');
    }

    /**
     * Finds and displays a Report entity.
     *
     * @Route("/{id}", name="report_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $report = $em->getRepository('AppBundle:Report')->find($id);

//        $states = $em->getRepository('AppBundle:ReportState')->find($id);
        $history = $em->getRepository('AppBundle:ReportState')->findBy(
            array('report' => $id), // criteria
            array('createdAt' => 'ASC') // order
        );

        $serializer = $this->container->get('serializer');
        $reportJSON = $serializer->serialize($report, 'json');

        if (!$report) {
            throw $this->createNotFoundException('Unable to find Report entity.');
        }

        return array(
            'report'            => $report,
            'history'           => $history,
            'loggedUser'        => $loggedUser,
            'reportJSON'        => $reportJSON,
        );
    }


}
