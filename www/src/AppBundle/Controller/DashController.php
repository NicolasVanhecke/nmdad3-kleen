<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Ob\HighchartsBundle\Highcharts\Highchart;

/**
 * Dash controller.
 *
 * @Route("/dash")
 */
class DashController extends Controller
{
    /**
     * @Route("/", name="dash")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($limit = 10)
    {
        $em = $this->getDoctrine()->getManager();

        $loggedUser = $this->getUser();
        $users = $em->getRepository('AppBundle:User')->findAll();
        $lastFiveUsers = $em->getRepository('AppBundle:User')->findBy(
            array(), // criteria
            array('createdAt' => 'DESC'), // order
            5, // limit
            0 // offset
        );
        $lastTenReports = $em->getRepository('AppBundle:Report')->findBy(
            array(), // criteria
            array('createdAt' => 'DESC'), // order
            10, // limit
            0 // offset
        );


        // Usercount
        // ----------
        $qbuilder = $em->createQueryBuilder();
        $qbuilder
            ->select('count(user.id)')
            ->from('AppBundle:User','user')
        ;
        $count = $qbuilder->getQuery()->getSingleScalarResult();


        // HighChart
        // ----------
        $series = array(
            array("name" => "Data Serie 1",    "data" => array(1,2,4,5,6,3,8)),
            array("name" => "Data Serie 2",    "data" => array(5,3,4,7,8,5,4))
        );

        $ob = new Highchart();
        $ob->chart->renderTo('linechart');  // The #id of the div where to render the chart
        $ob->title->text('Chart Title');
        $ob->xAxis->title(array('text'  => "Horizontal axis title"));
        $ob->yAxis->title(array('text'  => "Vertical axis title"));
        $ob->series($series);


//        // Logged in Check
//        // ----------
//        $isAuthenticated = $this->get('security.context')
//            ->isGranted('IS_AUTHENTICATED_REMEMBERED');
//
//        if (!$isAuthenticated) {
////            throw new AccessDeniedException();
//            return new RedirectResponse($this->generateUrl('security_login'));
//        }


        return array(
            'users'             => $users,
            'loggedUser'        => $loggedUser,
            'lastFiveUsers'     => $lastFiveUsers,
            'lastTenReports'    => $lastTenReports,
            'chart'             => $ob,
            'count'             => $count,
//            'qb'            => $qb,
        );
    }

    // For OB Charts (with highcharts js)
    public function chartAction()
    {

        return $this->render('AppBundle:Dash:index.html.twig', array(
        ));
    }
}
