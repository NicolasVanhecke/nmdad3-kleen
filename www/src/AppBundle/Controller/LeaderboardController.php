<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Leaderboard;
use Faker\Provider\tr_TR\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Leaderboard controller.
 *
 * @Route("/leaderboards")
 */
class LeaderboardController extends Controller
{
    /**
     * @Route("/", name="leaderboards")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $leaderboard = $em->getRepository('AppBundle:Leaderboard')->findBy(
            array(), // criteria
            array('completed' => 'DESC')
        );

        $users = $em->getRepository('AppBundle:User')->findAll();

        return array(
            'loggedUser'        => $loggedUser,
            'leaderboard'       => $leaderboard,
            'users'             => $users,
        );
    }

//    /**
//     * Change status from Report.
//     *
//     * @Route("/update/{status}/{id}", name="report_status")
//     * @Method("GET")
//     * @Template()
//     */
//    public function updateAction($id, $status)
//    {
//        $em = $this->getDoctrine()->getManager();
//        $report = $em->getRepository('AppBundle:Report')->find($id);
//
//        if (!$report) {
//            throw $this->createNotFoundException('Unable to find Report entity.');
//        }
//
//        $update = New ReportState();
//        $update ->setState($status)
//                ->setUserId(1)
//                ->setCreatedAt(New \DateTime)
//                ->setReport($report);
//        $em->persist($update);
//
//        $report->addReportstate($update);
//        $em->flush();
//
//        $message = 'Report ' . $id . ' is nu "' . $status . '"';
//        $this->addFlash('succes', $message);
//
//        return $this->redirectToRoute('reports');
//    }
//
//
//
//    /**
//     * Finds and displays a Report entity.
//     *
//     * @Route("/{id}", name="report_show")
//     * @Method("GET")
//     * @Template()
//     */
//    public function showAction($id)
//    {
//        $em = $this->getDoctrine()->getManager();
//        $loggedUser = $this->getUser();
//
//        $report = $em->getRepository('AppBundle:Report')->find($id);
//
////        $states = $em->getRepository('AppBundle:ReportState')->find($id);
//        $history = $em->getRepository('AppBundle:ReportState')->findBy(
//            array('report' => $id), // criteria
//            array('createdAt' => 'ASC'), // order
//            100, // limit
//            0 // offset
//        );
//
//        $serializer = $this->container->get('serializer');
//        $reportJSON = $serializer->serialize($report, 'json');
//
//        if (!$report) {
//            throw $this->createNotFoundException('Unable to find Report entity.');
//        }
//
//        return array(
//            'report'            => $report,
//            'history'           => $history,
//            'loggedUser'        => $loggedUser,
//            'reportJSON'        => $reportJSON,
//        );
//    }


}
