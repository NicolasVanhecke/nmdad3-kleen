<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template("AppBundle:Home:index.html.twig")
     */
    public function indexAction()
    {
//        $em = $this->getDoctrine()->getManager();

//        $posts = $em->getRepository('AppBundle:PostAbstract')->findAll();

        return [
//            'posts' => $posts,
        ];
    }
}
