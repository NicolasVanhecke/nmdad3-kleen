<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Badge;
use AppBundle\Form\Badge as BadgeForm;
use AppBundle\Form\BadgeType;

/**
 * Badge controller.
 *
 * @Route("/badges")
 */
class BadgeController extends Controller
{
    /**
     * @Route("/", name="badges")
     * @Template()
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $badges = $em->getRepository('AppBundle:Badge')->findAll();
        $users = $em->getRepository('AppBundle:User')->findAll();

        return array(
            'loggedUser'        => $loggedUser,
            'badges'            => $badges,
            'users'             => $users,
        );
    }

    /**
     * Creates a new Badge entity.
     *
     * @Route("/", name="badge_create")
     * @Method("POST")
     * @Template("AppBundle:Badge:new.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $badge = new Badge();
        $form = $this->createCreateForm($badge);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($badge);
            $em->flush();

            $message = 'Nieuwe badge is succesvol aangemaakt!';
            $this->addFlash('succes', $message);

            return $this->redirect($this->generateUrl('badges'));
        } else {
            $message = 'Kon badge niet aanmaken!';
            $this->addFlash('error', $message);
        }

        return array(
            'badge'             => $badge,
            'loggedUser'        => $loggedUser,
            'form'              => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Badge entity.
     *
     * @param Badge $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Badge $badge)
    {
        $formType = new BadgeForm\NewType();
        $form = $this->createForm($formType, $badge, [
            'action' => $this->generateUrl('badge_create'),
            'method' => 'POST',
        ]);

//        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->add('Maak badge aan', 'submit', array('attr' => array('class' => 'upc btn btn-submit')));

        return $form;
    }

    /**
     * Displays a form to create a new Badge entity.
     *
     * @Route("/new", name="badge_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $badge = new Badge();
        $form = $this->createCreateForm($badge);

        return array(
            'badge'             => $badge,
            'loggedUser'        => $loggedUser,
            'form'              => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Badge entity.
     *
     * @Route("/{id}/edit", name="badge_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $badge = $em->getRepository('AppBundle:Badge')->find($id);

        if (!$badge) {
            throw $this->createNotFoundException('Unable to find Badge entity.');
        }

        $message = 'Badge is succesvol aangepast!';
        $this->addFlash('succes', $message);

        $editForm = $this->createEditForm($badge);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'badge'             => $badge,
            'loggedUser'        => $loggedUser,
            'edit_form'         => $editForm->createView(),
            'delete_form'       => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Badge entity.
     *
     * @param Badge $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Badge $entity)
    {
        $form = $this->createForm(new BadgeType(), $entity, array(
            'action' => $this->generateUrl('badge_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

//        $form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('Opslaan', 'submit', array('attr' => array('class' => 'upc btn btn-submit')));

        return $form;
    }
    /**
     * Edits an existing Badge entity.
     *
     * @Route("/{id}", name="badge_update")
     * @Method("PUT")
     * @Template("AppBundle:Badge:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $badge = $em->getRepository('AppBundle:Badge')->find($id);

        if (!$badge) {
            throw $this->createNotFoundException('Unable to find Badge entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($badge);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('badges'));
        }

        return array(
            'badge'             => $badge,
            'loggedUser'        => $loggedUser,
            'edit_form'         => $editForm->createView(),
            'delete_form'       => $deleteForm->createView(),
        );
    }


    /**
     * Deletes a Badge entity.
     *
     * @Route("/{id}", name="badge_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $badge = $em->getRepository('AppBundle:Badge')->find($id);

            if (!$badge) {
                throw $this->createNotFoundException('Unable to find Badge entity.');
            }

            $em->remove($badge);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('badges'));
    }

    /**
     * Creates a form to delete a Badge entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('badge_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('badge verwijderen', 'submit', array('attr' => array('class' => 'upc btn btn-cancel')))
//            ->add('submit', 'submit', array('class' => 'btn btn-cancel'))
            ->getForm()
            ;
    }
}