<?php

namespace AppBundle\Controller;

use AppBundle\Traits\PasswordTrait;
use Doctrine\DBAL\Logging\DebugStack;
use Faker\Provider\tr_TR\DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Report;
use AppBundle\Entity\User;
use AppBundle\Form\User as UserForm;
use AppBundle\Form\UserType;

/**
 * User controller.
 *
 * @Route("/users")
 */
class UserController extends Controller
{
    use PasswordTrait;

    /**
     * Lists all User entities.
     *
     * @Route("/", name="users")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $users = $em->getRepository('AppBundle:User')->findAll();

//        $fakeReports = $em->getRepository('AppBundle:Report')->findBy(
//            array('state' => 'fake'), // criteria
//            array('createdAt' => 'ASC'), // order
//            5, // limit
//            0 // offset
//        );

        // Usercount
        // ----------
//        $qbuilder = $em->createQueryBuilder();
//        $qbuilder
//            ->select('count(user.id)')
//            ->from('AppBundle:User','user')
//        ;
//        $count = count($fakeReports);

        return array(
            'loggedUser'        => $loggedUser,
            'users'             => $users,
//            'fakeReports'       => $fakeReports,
//            'count'             => $count,
        );
    }


    /**
     * Creates a new User entity.
     *
     * @Route("/", name="users_create")
     * @Method("POST")
     * @Template("AppBundle:User:new.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $user = new User();
        $form = $this->createCreateForm($user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->hashPassword($user); // From AppBundle\Traits\PasswordTrait
            $this->addFlash('succes', 'Gebruiker succesvol aangemaakt!');

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('users'));
        }

        $this->addFlash('error', 'KON GEBRUIKER NIET AANMAKEN!');

        return array(
            'user'              => $user,
            'loggedUser'        => $loggedUser,
            'form'              => $form->createView(),
        );
    }


    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $user)
    {
        $formType = new UserForm\NewType();
        $form = $this->createForm($formType, $user, [
            'action' => $this->generateUrl('users_create'),
            'method' => 'POST',
        ]);

//        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->add('Maak gebruiker aan', 'submit', array('attr' => array('class' => 'upc btn btn-submit')));

        return $form;
    }


    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="users_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $user = new User();
        $form = $this->createCreateForm($user);

        return array(
            'user'              => $user,
            'loggedUser'        => $loggedUser,
            'form'              => $form->createView(),
        );
    }


    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="users_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $userReports = $em->getRepository('AppBundle:Report')->findBy(
            array('user' => $id), // criteria
            array('createdAt' => 'DESC') // order
        );


        $deleteForm = $this->createDeleteForm($id);

        return array(
            'user'              => $user,
            'loggedUser'        => $loggedUser,
            'delete_form'       => $deleteForm->createView(),
            'userReports'       => $userReports
        );
    }


    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="users_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($user);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'user'              => $user,
            'loggedUser'        => $loggedUser,
            'edit_form'         => $editForm->createView(),
            'delete_form'       => $deleteForm->createView(),
        );
    }


    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('users_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

//        $form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('Opslaan', 'submit', array('attr' => array('class' => 'upc btn btn-submit')));

        return $form;
    }


    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="users_update")
     * @Method("PUT")
     * @Template("AppBundle:User:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedUser = $this->getUser();

        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($user);
        $editForm->handleRequest($request);

//        if ($editForm->isValid()) {
            $message = 'Gebruiker is succesvol gewijzigd!';
            $this->addFlash('succes', $message);
            $em->flush();

            return $this->redirect($this->generateUrl('users_show', array('id' => $id)));
//        }

//        return array(
//            'user'              => $user,
//            'loggedUser'        => $loggedUser,
//            'edit_form'         => $editForm->createView(),
//            'delete_form'       => $deleteForm->createView(),
//        );
    }


    /**
     * SoftDelete user (set lockedAt with date).
     *
     * @Route("/softdelete/{id}", name="user_softdelete")
     * @Method("GET")
     * @Template()
     */
    public function softDeleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find Report entity.');
        }

        $user->setLockedAt(new \DateTime());
        $em->flush();

        return $this->redirectToRoute('users');
    }


    /**
     * SoftUndelete user (set lockedAt with date).
     *
     * @Route("/softundelete/{id}", name="user_softundelete")
     * @Method("GET")
     * @Template()
     */
    public function softUndeleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find Report entity.');
        }

        $user->setLockedAt(null);
        $em->flush();

        $message = 'Gebruiker is nu "unlocked"!';
        $this->addFlash('succes', $message);

        return $this->redirectToRoute('users');
    }


    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="users_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:User')->find($id);

            $message = 'Gebruiker verwijderd uit de database!';
            $this->addFlash('succes', $message);

            if (!$user) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($user);
            $em->flush();

            return $this->redirect($this->generateUrl('users'));
        }

        $message = 'Kon gebruiker niet verwijderen!';
        $this->addFlash('error', $message);

        return $this->redirect($this->generateUrl('users'));
    }


    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('gebruiker verwijderen', 'submit', array('attr' => array('class' => 'upc btn btn-cancel')))
//            ->add('submit', 'submit', array('class' => 'btn btn-cancel'))
            ->getForm()
            ;
    }
}