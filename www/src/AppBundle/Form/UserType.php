<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text' ,array('label' => 'Voornaam'))
            ->add('lastName', 'text' ,array('label' => 'Achternaam'))
            ->add('email', 'email' ,array('label' => 'E-mail'))
            ->add('birthday', 'date', [
                'label' => 'Geboortedatum',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd-mm-yyyy'
                ]])
            ->add('username', 'text' ,array('label' => 'Gebruikersnaam'))
            ->add('password')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_user';
    }
}



//            ->add('createdAt', 'date', [
//                    'widget' => 'single_text',
//                    'format' => 'dd-MM-yyyy',
//                    'attr' => [
//                        'class' => 'form-control input-inline datepicker',
//                        'data-provide' => 'datepicker',
//                        'data-date-format' => 'dd-mm-yyyy'
//                    ]])
//            ->add('enabledAt', 'date', [
//                    'widget' => 'single_text',
//                    'format' => 'dd-MM-yyyy',
//                    'attr' => [
//                        'class' => 'form-control input-inline datepicker',
//                        'data-provide' => 'datepicker',
//                        'data-date-format' => 'dd-mm-yyyy'
//                    ]])
//            ->add('lockedAt', 'date', [
//                    'widget' => 'single_text',
//                    'format' => 'dd-MM-yyyy',
//                    'attr' => [
//                        'class' => 'form-control input-inline datepicker',
//                        'data-provide' => 'datepicker',
//                        'data-date-format' => 'dd-mm-yyyy'
//                    ]])
//            ->add('expiredAt', 'date', [
//                    'widget' => 'single_text',
//                    'format' => 'dd-MM-yyyy',
//                    'attr' => [
//                        'class' => 'form-control input-inline datepicker',
//                        'data-provide' => 'datepicker',
//                        'data-date-format' => 'dd-mm-yyyy'
//                    ]])
//            ->add('expiredCredentialsAt', 'date', [
//                    'widget' => 'single_text',
//                    'format' => 'dd-MM-yyyy',
//                    'attr' => [
//                        'class' => 'form-control input-inline datepicker',
//                        'data-provide' => 'datepicker',
//                        'data-date-format' => 'dd-mm-yyyy'
//                    ]])
//            ->add('deletedAt', 'date', [
//                    'widget' => 'single_text',
//                    'format' => 'dd-MM-yyyy',
//                    'attr' => [
//                        'class' => 'form-control input-inline datepicker',
//                        'data-provide' => 'datepicker',
//                        'data-date-format' => 'dd-mm-yyyy'
//                    ]])

