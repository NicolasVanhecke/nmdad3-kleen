<?php

namespace AppBundle\Form\User;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text' ,array('label' => 'Voornaam'))
            ->add('lastName', 'text' ,array('label' => 'Achternaam'))
            ->add('email', 'email' ,array('label' => 'E-mail'))
            ->add('birthday', 'date', [
                'label' => 'Geboortedatum',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd-mm-yyyy'
                ]])
            ->add('username', 'text' ,array('label' => 'Gebruikersnaam'))
            ->add('passwordRaw', /* $type = */'repeated', /* $options = */[
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'required' => true,
                'first_options' => ['label' => 'Wachtwoord'],
                'second_options' => ['label' => 'Wachtwoord herhalen'],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getName()
    {
        return 'appbundle_user_update';
    }
}
