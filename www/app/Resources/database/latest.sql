-- MySQL dump 10.13  Distrib 5.6.19, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: kleen
-- ------------------------------------------------------
-- Server version	5.6.19-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `kleen`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `kleen` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `kleen`;

--
-- Table structure for table `badges`
--

DROP TABLE IF EXISTS `badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reports` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badges`
--

LOCK TABLES `badges` WRITE;
/*!40000 ALTER TABLE `badges` DISABLE KEYS */;
INSERT INTO `badges` VALUES (1,'Powerlevel: Over 9000',9000),(2,'Prestige',200),(3,'quos',7),(4,'et',3),(5,'veritatis',1),(6,'ducimus',6),(7,'pariatur',0);
/*!40000 ALTER TABLE `badges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboards`
--

DROP TABLE IF EXISTS `leaderboards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaderboards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `completed` int(11) NOT NULL,
  `dtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E974E117A76ED395` (`user_id`),
  CONSTRAINT `FK_E974E117A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaderboards`
--

LOCK TABLES `leaderboards` WRITE;
/*!40000 ALTER TABLE `leaderboards` DISABLE KEYS */;
INSERT INTO `leaderboards` VALUES (1,3,1,'leaderboard');
/*!40000 ALTER TABLE `leaderboards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_lat` decimal(10,6) NOT NULL,
  `location_long` decimal(10,6) NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `completed_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `dtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F11FA745A76ED395` (`user_id`),
  CONSTRAINT `FK_F11FA745A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reports`
--

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` VALUES (1,1,'vuilzakken','Drie vuilzakken',50.979797,3.784766,'base64String1','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(2,3,'repellendus','eos',51.030121,3.846460,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(3,4,'vel','eos',51.040756,3.767722,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(4,5,'reiciendis','vero',50.982564,3.781248,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(5,7,'quia','cupiditate',51.088480,3.701201,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(6,8,'asperiores','vitae',51.030524,3.638408,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(7,9,'inventore','tempora',51.159578,3.849517,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(8,10,'ducimus','nulla',51.072848,3.810274,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(9,12,'quaerat','voluptas',51.155515,3.808338,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(10,13,'omnis','rerum',50.982652,3.758558,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(11,15,'dolores','occaecati',51.145285,3.705079,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(12,16,'cupiditate','tempore',51.054607,3.810216,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(13,17,'quia','deserunt',51.099253,3.650027,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(14,18,'hic','et',50.986995,3.671849,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(15,19,'et','quia',51.041080,3.651478,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(16,20,'nesciunt','nulla',51.029503,3.758734,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(17,21,'molestiae','eum',51.111430,3.816000,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(18,22,'repellendus','dicta',51.189394,3.693583,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(19,23,'voluptatibus','quibusdam',51.102188,3.658178,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(20,24,'porro','nam',51.159305,3.706871,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(21,25,'atque','eum',51.000660,3.774106,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report'),(22,26,'similique','sed',51.146230,3.820105,'base64String','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','2015-12-13 15:59:08','report');
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reportstates`
--

DROP TABLE IF EXISTS `reportstates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportstates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report` int(10) unsigned NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `dtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_91E69CA8C42F7784` (`report`),
  CONSTRAINT `FK_91E69CA8C42F7784` FOREIGN KEY (`report`) REFERENCES `reports` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reportstates`
--

LOCK TABLES `reportstates` WRITE;
/*!40000 ALTER TABLE `reportstates` DISABLE KEYS */;
INSERT INTO `reportstates` VALUES (1,1,'opgeruimd','1','2015-12-13 15:59:08','reportstate'),(2,2,'gemeld','13','2015-12-13 15:59:08','reportstate'),(3,3,'gemeld','18','2015-12-13 15:59:08','reportstate'),(4,4,'gemeld','16','2015-12-13 15:59:08','reportstate'),(5,5,'in behandeling','4','2015-12-13 15:59:08','reportstate'),(6,6,'in behandeling','5','2015-12-13 15:59:08','reportstate'),(7,7,'in behandeling','8','2015-12-13 15:59:08','reportstate'),(8,8,'in behandeling','7','2015-12-13 15:59:08','reportstate'),(9,9,'fake','19','2015-12-13 15:59:08','reportstate'),(10,10,'fake','6','2015-12-13 15:59:08','reportstate'),(11,11,'opgeruimd','20','2015-12-13 15:59:08','reportstate'),(12,12,'opgeruimd','10','2015-12-13 15:59:08','reportstate'),(13,13,'opgeruimd','1','2015-12-13 15:59:08','reportstate'),(14,14,'opgeruimd','8','2015-12-13 15:59:08','reportstate'),(15,15,'opgeruimd','18','2015-12-13 15:59:08','reportstate'),(16,16,'opgeruimd','4','2015-12-13 15:59:08','reportstate'),(17,17,'opgeruimd','20','2015-12-13 15:59:08','reportstate'),(18,18,'opgeruimd','15','2015-12-13 15:59:08','reportstate'),(19,19,'opgeruimd','11','2015-12-13 15:59:08','reportstate'),(20,20,'opgeruimd','20','2015-12-13 15:59:08','reportstate'),(21,21,'opgeruimd','4','2015-12-13 15:59:08','reportstate'),(22,22,'opgeruimd','14','2015-12-13 15:59:08','reportstate'),(23,2,'in behandeling','1','2015-12-13 16:15:41','reportstate');
/*!40000 ALTER TABLE `reportstates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `enabled_at` datetime DEFAULT NULL,
  `locked_at` datetime DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `expired_credentials_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`),
  UNIQUE KEY `UNIQ_1483A5E9F85E0677` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Nicolas','Vanhecke','vanheckenicolas@me.com',NULL,'2015-12-13 15:59:05','2015-12-13 15:59:05',NULL,NULL,NULL,NULL,'NicolasV','$2y$10$iIAFZSrqW9WkF6GR/JJ9set7XMg6qPme6GmJk4/bKg8JuGrV5BHpC'),(2,'Robbe','Toussaint','pElAmrani@Robert.org',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'oDemir','$2y$10$q9bZtHlqnd0yyWBNH28UNecbmc9rxPlaI/zvN/7h8rF90tVhPM6g2'),(3,'Mila','Dubois','Bourgeois.Luca@hotmail.com',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Nicolas.Pierre','$2y$10$7yO/5M/lgjhJPcj976wYiuztNshu6uSTj9R5G.dnX6swVOx8SSSRu'),(4,'Adam','Pirotte','Jarne31@advalvas.be',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Axel.Rousseau','$2y$10$2O5l3ebMNb/x7oVCmJNBYu7yf95fyPAiHbSS6a9eEfiUTzVHZc4QC'),(5,'Elias','Piron','Thomas.Verschueren@gmail.com',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Senne.Lambert','$2y$10$tUNTMUynTwgLOHkFoubMlOvdPznzCeQ5lkZ1S.a9AXQx5Y5wcD7.O'),(6,'Jade','Diallo','Noemie14@hotmail.com',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'fKaya','$2y$10$NN47ZwDMzClKmByWQ0XdPu8mSjZRSqPfHLrLb.uP7JMjZ1HFT2SxK'),(7,'Mathis','Brasseur','Clemence60@yahoo.com',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'rPetit','$2y$10$U2AJ.GUvEuSPhfwSxpZAU.StZSXmCRq5uZC3iH73iDwcEJz4eCxiS'),(8,'Martin','Denis','Jeanne.Jansen@advalvas.be',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Delhaye.Hugo','$2y$10$93ETn5/dnmqOOqXXwHqec.RKcHKx2C0QKCW41n96UNVYrUuhL5PvG'),(9,'Robin','Mathieu','Jules28@Yildirim.org',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Borremans.Alyssa','$2y$10$0HOL8xZC5t0ksZzowkmRT.SneGBx7rb/FfyyqQ0VdJIUcY/iK1bDG'),(10,'Arne','Gillet','Tibo.Marechal@advalvas.be',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Samuel46','$2y$10$1iNe9BRhF1fCtYZWeC4RGOf.Y4aGKPHGXwCjVEi66ymPqKMDKehma'),(11,'Raphaël','Bogaert','Noe.Vermeulen@hotmail.com',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'eVervoort','$2y$10$G2vkPMycF7DNI.5ISPCMPely.gwThPan9jAB7o4HHeiCttyw6sxqy'),(12,'Antoine','Bosmans','Victor.DeCock@Bastin.be',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Victoria94','$2y$10$dOMYu4xZu2ephAwwis4IKe5QUFsce86AP7u3V.XeCPMHNMLqHvpom'),(13,'Wout','Marchal','Vermeersch.Zoe@yahoo.com',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Luca.Antoine','$2y$10$2CLIvNSPbNhWg.fm1Q5MHOkc51c.dO4IIqppjRMqRUYzT6fXe3rra'),(14,'Noa','Bastin','Verbeeck.Mats@hotmail.com',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Gilson.Charlotte','$2y$10$mRcCzsrfotBzLHLdBhjYLu/iRWf79KzB0pgHSmpNiaEiDcCM7.ZLu'),(15,'Lore','Smet','Declercq.Lena@advalvas.be',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'Arthur.Verstraete','$2y$10$AamAJoqXCCQ2RY6UYpdmEuNnnej.QXuh3221DmeUjqzEnUnz1XGZe'),(16,'Jeanne','Saidi','Vandenberghe.Rania@hotmail.com',NULL,'2015-12-13 15:59:06','2015-12-13 15:59:06',NULL,NULL,NULL,NULL,'DeGroote.Tristan','$2y$10$FlGKzqj5L6I5VASvb8K.uekXQWikekhsqUYtrwjYRCWvYJIIaQCbW'),(17,'Amy','Collignon','Lowie.Declercq@Segers.net',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Quinten42','$2y$10$OMUFVUPbom1DI.E7nH9ea.qo3oBv39rL7NYDd7wd5L/YSO/bcs.Ee'),(18,'Jarne','Goffin','Noe.Meunier@advalvas.be',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Hubert.Laure','$2y$10$22h6xag7idOTPVAD9B76eemfj5gXDo90Efqr/0xGJ/jlqWOd7MP6S'),(19,'Jana','Brasseur','Jeanne32@advalvas.be',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'uDevos','$2y$10$cl11O5McCGw/nFG1ASO.y.2ie1mk6WQWenF7wR6ZW3NdoXaLqrTS2'),(20,'David','Gilles','uSmet@advalvas.be',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'fJanssens','$2y$10$PW593.Oh1IpFNAZQJZzevuXQN31koOEA6NgVHivCTjQZWNMb/rISG'),(21,'Benjamin','Gérard','Desmet.Robbe@Janssens.com',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'wMoens','$2y$10$ev1qsZx00ee464ceumm3iOYcJGsaz12gzverCFlDmXGP0kaU3HVaG'),(22,'Mathis','Sahin','ElAmrani.Sara@Verbruggen.be',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'gGoffin','$2y$10$PiudGD1M4doBWC2hZeE0AOAmRYx6dTEdJjt0LNxXEo8sxKUuFZFBK'),(23,'Romane','Noël','oUrbain@Dogan.be',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Vermeiren.Sam','$2y$10$iifwZuCBY.Zzf1csErJsW.OyzduCpdyDUYuWrp5EyvrdGoNsYADEK'),(24,'Jonas','Benali','iDeVos@yahoo.com',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Adam.Romane','$2y$10$TNplWvzPkY9qNmJnqX7ot.h0YmaKScLL3wZZ9WddCZqLjN4dTDdeG'),(25,'Alicia','Wauters','Alyssa.Martin@gmail.com',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Bodart.Maya','$2y$10$muTODJBlQFizxewUZad02OOWnSe5sJA7uJAp6wa6/H1PT1L7n1j2S'),(26,'Robin','Dethier','DeConinck.Sacha@Verhoeven.net',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Clement51','$2y$10$J6PpwoyO1bdQhTpilwdrDOuPL8w5BJ9Pl5iP6Ov0C/gk3KQJlNzq2'),(27,'Nore','Antoine','Amber50@Bah.be',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Ethan.Pierre','$2y$10$GbB1ktBR33uOUI1WBgNeQ.wcSthL69mJZe/1KBPQrxrUvsc6jobZO'),(28,'Noor','De Wilde','Alessio39@advalvas.be',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Janssens.Elisa','$2y$10$oa9VY21Lq1HkXRz6B5PKLeXPWCBS7R1JvQbO058Pv1IIrjDm8Z2AO'),(29,'Arne','Van Hoof','yYildirim@Willems.net',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Linde.Descamps','$2y$10$/sH30/SiYuG6u0ObiL.K8udbCovfeLDuORXNrogYPvwMNgaTWZhyW'),(30,'Hanne','Ceulemans','Francois.Lore@gmail.com',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Rousseau.Sacha','$2y$10$hDSdoD4Bo2Pfv7.YXl3Q0OKbbw5pF9USvwyAf9wUhC.kj4IO/2LT2'),(31,'Lowie','Gérard','Julia41@Lauwers.com',NULL,'2015-12-13 15:59:07','2015-12-13 15:59:07',NULL,NULL,NULL,NULL,'Alexis09','$2y$10$KWWsTQY7O5tNtBIfQlBk3unUYD/HwbFTD4ZRCxCPULo0OoMIVzsi2'),(32,'Niels','Robert','qVanAcker@gmail.com',NULL,'2015-12-13 15:59:08','2015-12-13 15:59:08',NULL,NULL,NULL,NULL,'Guillaume18','$2y$10$zzpqOdcQa7gf3OIaeY3xzukME1XxAMsg3XjXxMbfUkr/RW84I4bsy'),(33,'Eva','Verbruggen','Mohamed.Diallo@Luyten.be',NULL,'2015-12-13 15:59:08','2015-12-13 15:59:08',NULL,NULL,NULL,NULL,'uLejeune','$2y$10$4g9LGzVcxVkkuhOzcDCsFuLzeyO4UMr8zNtkbaBzh31Pr7LXjhUWS'),(34,'Maya','Declercq','Camille77@Thijs.be',NULL,'2015-12-13 15:59:08','2015-12-13 15:59:08',NULL,NULL,NULL,NULL,'Jonas.Marechal','$2y$10$LemuJKwnO6c/kZddMUSkKes99GK8ZAxFi72IeaSvQVU6vmpC7PvMa'),(35,'Maëlle','Lievens','Ruben56@gmail.com',NULL,'2015-12-13 15:59:08','2015-12-13 15:59:08',NULL,NULL,NULL,NULL,'kHendrickx','$2y$10$8xjgBfZBPQA7dhIs.Vi71.p.RKNtLn1xmzPKGxvk2Y3Fp/8TmMQZW'),(36,'Benjamin','Dupont','Leon.Thys@Tran.org',NULL,'2015-12-13 15:59:08','2015-12-13 15:59:08',NULL,NULL,NULL,NULL,'Maxime.Herman','$2y$10$EbFS3EQMFbQGmbODInjPpOW.BCL4QHFvfX6tipTOXasn5eK1ZtZOi'),(37,'nicolasvh','nicolasvh','nicolasvh@me.com','2015-12-13 00:00:00','2015-12-13 16:01:22','2015-12-13 16:01:22',NULL,NULL,NULL,NULL,'nicolasvh','$2y$10$CanRCbINJJP0qoqVyzejZ.HmVVWN.Wuq1FMUf/cpyjlZ.taW3RNBu');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-15 17:54:39
