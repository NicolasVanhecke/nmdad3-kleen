var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
  gulp.src('./src/AppBundle/Resources/scss/symfony.app.scss')
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(gulp.dest('./web/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./web/css/'))
    .on('end', done);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});




/**
 * Custom Gulp Tasks.
 */
var paths = {
    sass: ['./src/AppBundle/Resources/scss/**/*.scss']
};

gulp.task('watch', function() {
    gulp.watch(paths.sass, ['sass']);
});

// Build the application.
gulp.task('build', ['scripts', 'bootstrap', 'bootstrapjs', 'jquery'], build);
function build() {
    console.log(gutil.colors.blue.bgWhite(' App succesfully built. '));
}


/**
 * SEPERATE TASKS.
 */
// Concatenate all scripts to `./www/js/app.js`.
gulp.task('scripts', scripts);
function scripts() {
    return gulp
        .src('./src/AppBundle/Resources/js/**/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./web/js'));
}

// Load Bootstrap from vendor folder
gulp.task('bootstrap', function(done) {
    gulp.src('./vendor/twbs/bootstrap/dist/css/bootstrap.min.css')
        .pipe(gulp.dest('./web/css/bootstrap/'))
        .on('end', done);
});

// Load Bootstrap from vendor folder
gulp.task('bootstrapjs', function(done) {
    gulp.src('./vendor/twbs/bootstrap/dist/js/bootstrap.min.js')
        .pipe(gulp.dest('./web/js/bootstrap/'))
        .on('end', done);
});

// Load jQuery from vendor folder
gulp.task('jquery', function(done) {
    gulp.src('./vendor/components/jquery/jquery.min.js')
        .pipe(gulp.dest('./web/js/jquery/'))
        .on('end', done);
});
